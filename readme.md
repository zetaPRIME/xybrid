![Xybrid logo](asset-work/xybrid-logo-banner480.png)
Xybrid: deeply modular tracker

## Build dependencies:
- Qt 5.12 or later

## License
The Software (all C/C++ code and bundled Lua or JavaScript, excepting where otherwise specified) is made available under the terms of the [GNU Lesser General Public License, v2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html) or later.

The Assets (images and image project files (`*.xcf`), as well as all bundled example projects, excepting where otherwise specified) are licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
