#pragma once

#include "nodelib/instrumentcore.h"

namespace Xybrid::Instruments {
    class Thicc : public Data::Node {
        NodeLib::InstrumentCore core;

        NodeLib::ADSR adsr;

        int wave = 0;
        int voices = 1;

        double mod = 0.0;
        double detune = 0.0;

    public:
        Thicc();
        ~Thicc() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
