#pragma once

#include "nodelib/instrumentcore.h"

namespace Xybrid::Instruments {
    class I2x03 : public Data::Node {
        NodeLib::InstrumentCore core;

        NodeLib::ADSR adsr;

        double detune = 0.0;

        int8_t wave = 0;
        int8_t blipWave = -1;

        int blipNote = 0;
        double blipTime = 0.0;
        double pwmTime = 3.0;
        double pwmDepth = 0.75;
        double pwmPhase = 0.0;
    public:
        I2x03();
        ~I2x03() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;
        //void onDoubleClick() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}
