#pragma once

#include "nodelib/instrumentcore.h"

namespace Xybrid::Instruments {
    class Xriek : public Data::Node {
        NodeLib::InstrumentCore core;

        NodeLib::ADSR adsr;

        double drive = 0.0;
        double saturation = 0.0;
    public:
        Xriek();
        ~Xriek() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
