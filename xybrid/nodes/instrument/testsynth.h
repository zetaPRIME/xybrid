#pragma once

#include "data/node.h"

namespace Xybrid::Instruments {
    class TestSynth : public Data::Node {
        //
        double osc = 0;
        double note [[maybe_unused]] = 45+12;
        //double lfo = 0;

        uint16_t noteId = 0;
        double cvol = 0;
        double tvol = 0;
    public:
        TestSynth();
        ~TestSynth() override = default;

        void init() override;
        void reset() override;
        void process() override;

        //void onRename() override;

        //void saveData(QCborMap&) const override;
        //void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;
        void onDoubleClick() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}

