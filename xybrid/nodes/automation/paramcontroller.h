#pragma once

#include <cmath>
#include <limits>

#include "data/node.h"

namespace Xybrid::Automation {
    class ParamController : public Data::Node {
        char cmd = ' ';
        bool sgn = false;

        double start = std::numeric_limits<double>::quiet_NaN();
        double last = 0.0;

        double target = 0.0;
        int ticksLeft = -1;

        inline double conv(uint8_t i) const {
            if (sgn) return std::clamp(static_cast<double>(static_cast<int8_t>(i)) / 127.0, -1.0, 1.0);
            return std::clamp(static_cast<double>(i) / 255.0, 0.0, 1.0);
        }

    public:
        ParamController() = default;
        ~ParamController() override = default;

        void init() override;
        void reset() override;
        //void release() override;
        void process() override;

        void onPortConnected(Data::Port::Type, Data::Port::DataType, uint8_t, std::weak_ptr<Data::Port>) override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
