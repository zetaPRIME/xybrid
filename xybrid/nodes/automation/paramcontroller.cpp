#include "paramcontroller.h"

using Xybrid::Automation::ParamController;
using namespace Xybrid::Data;

#include "util/strings.h"

#include "nodelib/basics.h"
#include "nodelib/commandreader.h"
using namespace Xybrid::NodeLib;

#include "data/audioframe.h"
#include "data/porttypes.h"

#include "config/pluginregistry.h"
using namespace Xybrid::Config;

#include "audio/audioengine.h"
using namespace Xybrid::Audio;

#include "ui/patchboard/nodeobject.h"
#include "ui/gadgets/layoutgadget.h"
#include "ui/gadgets/togglegadget.h"
#include "ui/gadgets/knobgadget.h"
#include "ui/gadgets/paramselectorgadget.h"
using namespace Xybrid::UI;

#include <cmath>

#include <QCborMap>

// clazy:excludeall=non-pod-global-static
RegisterPlugin(ParamController, {
    i->id = "auto:paramcontroller";
    i->displayName = "Param Controller";
    i->category = PluginRegistry::CATEGORY_AUTOMATION;
})


void ParamController::init() {
    addPort<CommandPort>(Port::Input, 0);
    addPort<CommandPort>(Port::Output, 0)->name = qs("stripped");
    addPort<ParameterPort>(Port::Output, 0);
}

namespace {
    void clear(ParamReader& pr) { pr.param() = 0; pr.val() = 0; }
    const constexpr double dnan = std::numeric_limits<double>::quiet_NaN();
}

void ParamController::reset() {
    last = 0.0;
    ticksLeft = -1;
    start = dnan;
}

void ParamController::process() {
    auto in = port<CommandPort>(Port::Input, 0);
    auto pt = port<CommandPort>(Port::Output, 0);
    auto out = port<ParameterPort>(Port::Output, 0);

    in->pull();
    auto ptc = pt->isConnected();
    if (ptc) pt->push(in);
    if (cmd == ' ') return; // no op on strut

    CommandReader cr(ptc ? pt : in);

    while (++cr) {
        ParamReader pr(cr);
        bool ers = false;
        while (++pr) {
            if (pr.isExt()) {
                if (ers) clear(pr);
                continue;
            }

            ers = false; // reset erase flag
            if (pr.param() == cmd) {
                ers = ptc; // erase following exts after finding our target
                auto val = pr.val();
                auto tw = pr.tweenInt();
                if (tw >= 1) { // tween
                    ticksLeft = tw;
                    target = conv(val);
                    if (std::isnan(start)) start = last;
                } else { // not tween
                    ticksLeft = 0;
                    start = conv(val);
                    last = start;
                }
            }
        }
    }

    // handle outputs and tweens
    out->pull();
    (*out)[0] = start;
    if (ticksLeft > 0) { // tweening
        auto ts = audioEngine->curTickSize();
        auto dts = static_cast<double>(ts);
        auto tlp = 1.0 / static_cast<double>(ticksLeft);
        for (size_t i = 1; i < ts; ++i) {
            auto tv = std::lerp(start, target, (static_cast<double>(i) / dts) * tlp);
            (*out)[i] = tv;
        }
        start = std::lerp(start, target, tlp);
        last = start;
        --ticksLeft;
    } else if (ticksLeft == 0) {
        start = dnan;
        ticksLeft = -1;
    }
}

void ParamController::onPortConnected(Port::Type, Port::DataType dataType, uint8_t, std::weak_ptr<Data::Port> op) {
    if (dataType == Port::Parameter) {
        if (auto p = op.lock(); p) {
            if (auto ps = p->getProperty("signed"); !ps.isNull()) {
                sgn = ps.toBool();
                if (obj) obj->update();
            }
        }
    }
}

void ParamController::saveData(QCborMap& m) const {
    m[qs("cmd")] = cmd;
    m[qs("signed")] = sgn;
}

void ParamController::loadData(const QCborMap& m) {
    cmd = m.value("cmd").toInteger(' ');
    sgn = m.value("signed").toBool(false);
}

void ParamController::onGadgetCreated() {
    if (!obj) return;
    auto l = new LayoutGadget(obj);
    l->setMetrics(10, 10);

    (new ParamSelectorGadget(l))->bind(cmd);
    (new ToggleGadget(l))->bind(sgn)->setColor({191,127,255})->setToolTip(qs("Signed"));
}

