#pragma once

#include "nodelib/instrumentcore.h"
#include "data/audioframe.h"

namespace Xybrid::Data { class Sample; }
namespace Xybrid::Instruments {
    class Capaxitor : public Data::Node {
        NodeLib::InstrumentCore core;

        struct NoteData {
            double sampleTime = 0;

            NoteData() = default;
            ~NoteData() = default;
        };
        static_assert (sizeof(NoteData) <= sizeof(NodeLib::InstrumentCore::Note::scratch), "Note data overflows scratch space!");

        std::weak_ptr<Data::Sample> smp = std::weak_ptr<Data::Sample>();

        NodeLib::ADSR adsr;



    public:
        Capaxitor();
        ~Capaxitor() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;

        void onDoubleClick() override;
        //void initUI(UI::NodeUIScene*) override;
    };
}
