#pragma once

#include "nodelib/instrumentcore.h"

namespace Xybrid::Data { class Sample; }
namespace Xybrid::Instruments {
    class BeatPad : public Data::Node {
        NodeLib::InstrumentCore core;

        struct NoteConfig {
            std::weak_ptr<Data::Sample> smp = std::weak_ptr<Data::Sample>();
            ptrdiff_t start = -1;
            ptrdiff_t end = -1;

            double gain = 0.0;
        };

        struct NoteData {
            std::shared_ptr<NoteConfig> config = nullptr;
            double sampleTime = 0;

            NoteData() = default;
            ~NoteData() = default;
        };
        static_assert (sizeof(NoteData) <= sizeof(NodeLib::InstrumentCore::Note::scratch), "Note data overflows scratch space!");

        std::unordered_map<int16_t, std::shared_ptr<NoteConfig>> cfg;

    public:
        BeatPad();
        ~BeatPad() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;

        void onDoubleClick() override;
        void initUI(UI::NodeUIScene*) override;
    };
}
