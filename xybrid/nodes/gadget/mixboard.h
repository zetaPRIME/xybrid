#pragma once

#include <vector>

#include "data/node.h"

namespace Xybrid::Gadgets {
    class MixBoard : public Data::Node {
        //
    public:
        struct Section {
            bool mute = false;
            bool solo = false;
            double gain = 0;
        };

        std::vector<Section> sections;

        MixBoard();
        ~MixBoard() override = default;

        void init() override;
        //void reset() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void insertSection(uint8_t);
        void removeSection(uint8_t);

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}
