#pragma once

#include <atomic>

#include "data/node.h"

namespace Xybrid::Gadgets {
    class GainBalance : public Data::Node {
        std::atomic<double> gain = 0.0;
        std::atomic<double> balance = 0.0;
    public:
        GainBalance();
        ~GainBalance() override = default;

        void init() override;
        //void reset() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}
