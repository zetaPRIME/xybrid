#pragma once

#include "data/node.h"

namespace Xybrid::Gadgets {
    class IOPort : public Data::Node {
        bool portSet = false;
        Data::Port::Type type;
        Data::Port::DataType dataType;
        uint8_t index;

        void remove();
        void add();
    public:
        IOPort();
        ~IOPort() override = default;

        void setPort(Data::Port::Type type, Data::Port::DataType dataType, uint8_t index);

        bool isVolatile() const override { return true; }

        void process() override;

        void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onUnparent(std::shared_ptr<Data::Graph>) override;
        void onParent(std::shared_ptr<Data::Graph>) override;

        void onDoubleClick() override;

        void onGadgetCreated() override;

        void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}
