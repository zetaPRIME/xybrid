#include "transpose.h"
using Xybrid::Gadgets::Transpose;
using namespace Xybrid::Data;

#include "util/strings.h"

#include "data/porttypes.h"

#include "config/pluginregistry.h"
using namespace Xybrid::Config;

#include "audio/audioengine.h"
using namespace Xybrid::Audio;

#include "nodelib/commandreader.h"
using namespace Xybrid::NodeLib;

#include "ui/patchboard/nodeobject.h"
#include "ui/gadgets/layoutgadget.h"
#include "ui/gadgets/knobgadget.h"
using namespace Xybrid::UI;

#include <cmath>

#include <QCborMap>

// clazy:excludeall=non-pod-global-static
RegisterPlugin(Transpose, {
    i->id = "gadget:transpose";
    i->displayName = "Transpose";
    i->category = PluginRegistry::CATEGORY_GADGET;
})

Transpose::Transpose() {

}

void Transpose::init() {
    addPort(Port::Input, Port::Command, 0);
    addPort(Port::Output, Port::Command, 0);
}

void Transpose::process() {
    int off = octave.load()*12 + amount.load();

    auto in = std::static_pointer_cast<CommandPort>(port(Port::Input, Port::Command, 0));
    auto out = std::static_pointer_cast<CommandPort>(port(Port::Output, Port::Command, 0));
    if (off == 0) { // a
        if (out->passthroughTo.expired()) out->passthroughTo = in;
        return;
    }
    if (!out->passthroughTo.expired()) out->passthroughTo.reset();
    in->pull();
    out->push(in); // copy buffer for direct modification
    CommandReader cr(out);

    while (++cr) { // step through note data
        auto& n = cr.note();
        if (n > -1) { // if there's actually a note here...
            n += off; // add the offset
            if (n < 0 || n > 32767) n = -1; // and cancel invalid notes
        }
    }
}

void Transpose::saveData(QCborMap& m) const {
    m[qs("amount")] = QCborValue(amount);
    m[qs("octave")] = QCborValue(octave);
}

void Transpose::loadData(const QCborMap& m) {
    auto oct = m.value("octave");
    if (oct.isUndefined()) { // convert from single value
        int a = static_cast<int>(m.value("amount").toInteger(0));
        int s = a < 0 ? -1 : 1;
        a = std::abs(a);
        octave = (a-(a%12))/12*s;
        amount = a%12 * s;
        return;
    }
    octave = static_cast<int>(oct.toInteger(0));
    amount = static_cast<int>(m.value("amount").toInteger(0));
}

void Transpose::onGadgetCreated() {
    if (!obj) return;
    obj->showPluginName = false;
    auto l = (new LayoutGadget(obj))->setMetrics(8, 10);

    (new KnobGadget(l))->bind(amount)->setLabel("Transpose")->setTextFunc(KnobGadget::textOffset)->setRange(-12, 12, 1, KnobGadget::MedStep);
    (new KnobGadget(l))->bind(octave)->setLabel("Octave")->setTextFunc(KnobGadget::textOffset)->setRange(-5, 5, 1, KnobGadget::BigStep);
}
