#pragma once

#include <array>

#include <QContiguousCache>

#include "data/node.h"
#include "data/audioframe.h"

namespace Xybrid::Gadgets {
    class QuickLevel : public Data::Node {
        QContiguousCache<Data::AudioFrame> buf;

        std::array<std::array<double, 2>, 2> lv;
    public:
        // time across which the displayed levels are calculated
        static const constexpr double SPAN_TIME = 1.0/30;
        static const constexpr double DISPLAY_EXPONENT = 1;

        QuickLevel();
        ~QuickLevel() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        void onPortDisconnected(Data::Port::Type, Data::Port::DataType, uint8_t, std::weak_ptr<Data::Port>) override;

        void onGadgetCreated() override;
        void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;

    };
}
