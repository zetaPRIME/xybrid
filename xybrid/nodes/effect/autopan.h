#pragma once

#include "data/node.h"

namespace Xybrid::Effects {
    class AutoPan : public Data::Node {
        double level;
        double rate = 1.0;
        double phase = 0.0;
        bool bpmRelative = false;

        double cyc; // current cycle tracking

    public:
        AutoPan();
        ~AutoPan() override = default;

        void init() override;
        void reset() override;
        //void release() override;
        void process() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
