#pragma once

#include "data/node.h"
#include "nodelib/parameter.h"

namespace Xybrid::Effects {
    class Distortion : public Data::Node {
        NodeLib::Parameter drive = {"Drive", 0.0, 24.0, 0.0};
        NodeLib::Parameter shape = {"Shape", -10.0, 10.0, 0.0};
        NodeLib::Parameter mix = {"Mix", 0.0, 1.0, 1.0};
        NodeLib::Parameter output = {"Output", -12.0, 12.0, 0.0};

    public:
        Distortion();
        ~Distortion() override = default;

        void init() override;
        void reset() override;
        //void release() override;
        void process() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
