#pragma once

#include "data/node.h"
#include "nodelib/parameter.h"

#include "inclib/freeverb/revmodel.hpp"

namespace Xybrid::Effects {
    class Reverb : public Data::Node {
        NodeLib::Parameter rsize = {"Room Size", 0.0, 1.0, 0.75};
        NodeLib::Parameter damp = {"Damp", 0.0, 1.0, 0.5};
        NodeLib::Parameter width = {"Width", 0.0, 1.0, 0.5};
        NodeLib::Parameter dry = {"Dry", 0.0, 1.0, 1.0};
        NodeLib::Parameter wet = {"Wet", 0.0, 1.0, 0.5};

        revmodel fvb;

    public:
        Reverb();
        ~Reverb() override = default;

        void init() override;
        void reset() override;
        //void release() override;
        void process() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
