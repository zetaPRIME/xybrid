#include "reverb.h"

using Xybrid::Effects::Reverb;
using namespace Xybrid::Data;

#include "util/strings.h"

#include "nodelib/basics.h"
using namespace Xybrid::NodeLib;

#include "data/audioframe.h"
#include "data/porttypes.h"

#include "config/pluginregistry.h"
using namespace Xybrid::Config;

#include "audio/audioengine.h"
using namespace Xybrid::Audio;

#include "ui/patchboard/nodeobject.h"
#include "ui/gadgets/layoutgadget.h"
#include "ui/gadgets/togglegadget.h"
#include "ui/gadgets/knobgadget.h"
using namespace Xybrid::UI;

#include <cmath>

#include <QCborMap>

// clazy:excludeall=non-pod-global-static
RegisterPlugin(Reverb, {
    i->id = "fx:reverb";
    i->displayName = "Reverb";
    i->category = PluginRegistry::CATEGORY_EFFECT;
})

Reverb::Reverb() = default;

void Reverb::init() {
    addPort(Port::Input, Port::Audio, 0);
    addPort(Port::Output, Port::Audio, 0);
}

void Reverb::reset() {
    fvb.mute(); // clear buffers
}

void Reverb::process() {
    auto in = port<AudioPort>(Port::Input, 0);
    auto out = port<AudioPort>(Port::Output, 0);
    { in->pull(); out->pull(); }

    // set freeverb model parameters
    fvb.setroomsize(rsize);
    fvb.setdamp(damp);
    fvb.setwidth(width);
    // wet/dry proportional to freeverb scale factor
    // (100% wet 0% dry should be perceptually unchanged)
    fvb.setdry(dry / scaledry);
    fvb.setwet(wet / scaledry);

    // and have it operate directly on our in/out buffers
    auto ts = audioEngine->curTickSize();
    fvb.processreplace(in->bufL, in->bufR, out->bufL, out->bufR, ts, 1);
}

void Reverb::saveData(QCborMap& m) const {
    rsize.save(m);
    damp.save(m);
    width.save(m);
    dry.save(m);
    wet.save(m);
}

void Reverb::loadData(const QCborMap& m) {
    rsize.load(m);
    damp.load(m);
    width.load(m);
    dry.load(m);
    wet.load(m);
}

void Reverb::onGadgetCreated() {
    if (!obj) return;
    auto l = new LayoutGadget(obj);

    KnobGadget::autoPercent(l, rsize);//->setSize(34);
    KnobGadget::autoPercent(l, damp);
    KnobGadget::autoPercent(l, width);
    const constexpr double dwsize = 24;
    KnobGadget::autoPercent(l, dry)->setSize(dwsize);
    KnobGadget::autoPercent(l, wet)->setSize(dwsize);
}
