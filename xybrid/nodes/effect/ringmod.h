#pragma once

#include "data/node.h"

namespace Xybrid::Effects {
    class RingMod : public Data::Node {
        double mix = 1.0;
        bool am = false;

    public:
        RingMod();
        ~RingMod() override = default;

        void init() override;
        void reset() override;
        //void release() override;
        void process() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        void onGadgetCreated() override;
    };
}
