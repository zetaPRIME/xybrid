/*
 * Filename:    svf.h
 *
 * Description:
 *
 *
 * Version:
 * Created:     Fri Nov  1 23:34:34 2019
 * Revision:    None
 * Author:      Rachel Fae Fox (foxiepaws),fox@foxiepa.ws
 *
 */


#pragma once

#include "data/node.h"
#include "data/audioframe.h"
#include "nodelib/svfilter.h"
#include "nodelib/parameter.h"

namespace Xybrid::Effects {
    class SVF : public Data::Node {
        NodeLib::SVFilter filter;

        //double cutoff = 6440.0;
        NodeLib::Parameter cutoff = {"Cutoff", 0, 16000, 0};

        double resonance = 0.0;

    public:
        enum FilterMode : uchar { Off, Low, High, Band, Notch };
        FilterMode mode = Low;

        SVF();
        ~SVF() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}
