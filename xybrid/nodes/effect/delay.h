#pragma once

#include <QContiguousCache>

#include "data/node.h"
#include "data/audioframe.h"

namespace Xybrid::Effects {
    class Delay : public Data::Node {
        QContiguousCache<Data::AudioFrame> buf;

        double delayTime = 0.5;
        bool bpmRelative = false;

        bool pingPong = false;

        double amount = 0.5;
        double feedback = 0.0;

    public:
        Delay();
        ~Delay() override = default;

        void init() override;
        void reset() override;
        void release() override;
        void process() override;

        //void onRename() override;

        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //void onUnparent(std::shared_ptr<Data::Graph>) override;
        //void onParent(std::shared_ptr<Data::Graph>) override;

        void onGadgetCreated() override;

        //void drawCustomChrome(QPainter*, const QStyleOptionGraphicsItem*) override;
    };
}
