#include "audioconfig.h"
#include "uiconfig.h"
#include "colorscheme.h"
#include "directories.h"

#include <QStandardPaths>

#include "theme/patchboard/defaultstyle.h"

using namespace Xybrid::Config;
using namespace Xybrid::Theme;

// Audio defaults
int AudioConfig::playbackSampleRate = 48000;
int AudioConfig::playbackBufferMs = 64;

int AudioConfig::previewSampleRate = 48000;
int AudioConfig::previewBufferMs = 64;

int AudioConfig::renderSampleRate = 48000;

// UIConfig defaults
bool UIConfig::verticalKnobs = false;
bool UIConfig::invertScrollWheel = false;
bool UIConfig::modplugOctaveNotation = false;

std::shared_ptr<PatchboardStyle> UIConfig::patchboardStyle = std::make_shared<Patchboard::DefaultStyle>();

// instantiate color scheme
ColorScheme Xybrid::Config::colorScheme;

// Directories
const QString Directories::configFile = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation).append("/xybrid/config.dat");
const QString Directories::stateFile = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation).append("/xybrid/state.dat");

QString Directories::projects = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation).append("/xybrid/projects");
QString Directories::presets = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation).append("/xybrid/nodes");
QString Directories::userDefaultTemplate = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation).append("/xybrid/default.xyp");

