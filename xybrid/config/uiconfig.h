#pragma once

#include <memory>

namespace Xybrid::Theme { class PatchboardStyle; }
namespace Xybrid::Config {
    namespace UIConfig {
        /// Determines if KnobGadgets turn with vertical mouse movement instead of horizontal.
        extern bool verticalKnobs;

        /// Controls if scroll wheel function is inverted for knobs, etc.
        extern bool invertScrollWheel;

        /// OpenMPT calls MIDI note 0 "C0" (Yamaha pitch) but in the actual GM standard that's octave -1 (Roland pitch).
        extern bool modplugOctaveNotation;

        // // // // // //
        // theme stuff //
        // // // // // //

        extern std::shared_ptr<Theme::PatchboardStyle> patchboardStyle;
    }
}
