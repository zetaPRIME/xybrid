#pragma once

#include <list>

#include <QString>

namespace Xybrid::Config {
    namespace UIState {
        const constexpr size_t MAX_RECENTS = 10;
        extern std::list<QString> recentFiles;

        void save();
        void addRecentFile(const QString& f);

        extern QString lastProjectDirectory;
        QString getProjectDirectory();
        void setProjectDirectory(const QString&);
    }
}
