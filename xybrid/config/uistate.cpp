#include "uistate.h"

#include <algorithm>

#include <QFileInfo>
#include <QDir>

#include "fileops.h"
#include "config/directories.h"
using namespace Xybrid::Config;

std::list<QString> UIState::recentFiles;

void UIState::save() {
    FileOps::saveUIState();
}

void UIState::addRecentFile(const QString &f) {
    if (!recentFiles.empty() && recentFiles.front() == f) return; // if it's already the most recent file, skip
    recentFiles.remove(f); // remove any existing instance from later in the list
    recentFiles.push_front(f);
    while (recentFiles.size() > MAX_RECENTS) recentFiles.pop_back(); // trim to max size
    save(); // and save changes
}

QString UIState::lastProjectDirectory;

QString UIState::getProjectDirectory() {
    if (lastProjectDirectory.isEmpty()) return Directories::projects;
    return lastProjectDirectory;
}

void UIState::setProjectDirectory(const QString& fn) {
    QFileInfo fi(fn);
    auto dir = fi.dir();
    lastProjectDirectory = dir.absolutePath();
}
