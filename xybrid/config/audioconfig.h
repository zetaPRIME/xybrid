#pragma once

namespace Xybrid::Config {
    namespace AudioConfig {
        extern int playbackSampleRate;
        extern int playbackBufferMs;

        extern int previewSampleRate;
        extern int previewBufferMs;

        extern int renderSampleRate;
    }
}
