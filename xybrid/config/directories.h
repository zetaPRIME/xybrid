#pragma once

#include <QString>

namespace Xybrid::Config {
    namespace Directories {
        const extern QString configFile;
        const extern QString stateFile;

        extern QString projects;
        extern QString presets;
        extern QString userDefaultTemplate;
    }
}
