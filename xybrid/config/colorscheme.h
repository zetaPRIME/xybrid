#pragma once

#include <QColor>

namespace Xybrid::Config {
    class ColorScheme {
    public:
        ColorScheme() = default;

        QColor patternSelection = {127, 63, 255, 63};
        QColor patternFoldIndicator = {95, 95, 95};
        QColor patternFoldShade = {0, 0, 0, 31};
        QColor patternOuterShade = {0, 0, 0, 63};

        QColor patternBg = {23, 23, 23};
        QColor patternBgBeat = {31, 31, 31};
        QColor patternBgMeasure = {39, 39, 39};

        QColor patternFgBlank = {127, 127, 127};
        QColor patternFgPort = {191, 191, 191};
        QColor patternFgNote = {255, 255, 255};
        QColor patternFgParamCmd = {191, 163, 255};
        QColor patternFgParamAmt = {191, 222, 255};

        QColor waveformBg = {23, 23, 23};
        QColor waveformBgHighlight = {31, 31, 47};
        QColor waveformFgPrimary = {191, 163, 255};
        QColor waveformLoopPoints = {255, 127, 127};
    };
    extern ColorScheme colorScheme;
}
