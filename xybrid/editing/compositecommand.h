#pragma once

#include <memory>
#include <vector>

#include <QUndoCommand>

namespace Xybrid::Data {
    class Project;
    class Pattern;
}

namespace Xybrid::Editing {
    class ProjectCommand;
    class PatternCommand;
    class CompositeCommand : public QUndoCommand {
        std::shared_ptr<Data::Project> project;
        std::shared_ptr<Data::Pattern> pattern;
        std::vector<std::unique_ptr<QUndoCommand>> children;

        int refreshLevel = -1;

    public:
        CompositeCommand() = default;
        CompositeCommand(std::shared_ptr<Data::Project>);
        ~CompositeCommand() override = default;

        CompositeCommand* reserve(int);

        CompositeCommand* compose(ProjectCommand*);
        CompositeCommand* compose(PatternCommand*);

        bool commit(QString name = QString());
        bool cancel();

        void redo() override;
        void undo() override;
    };
}
