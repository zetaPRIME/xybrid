#pragma once

#include "data/node.h"
#include "data/audioframe.h"
#include "audio/audio.h"

namespace Xybrid::Data {
    class AudioPort : public Port {
    public:
        class FrameRef {
            friend class AudioPort;
            AudioPort* port;
            size_t at;
            FrameRef(AudioPort* port, size_t at) : port(port), at(at) { }
        public:
            FrameRef& operator=(AudioFrame f) {
                port->bufL[at] = static_cast<Audio::bufferType>(f.l);
                port->bufR[at] = static_cast<Audio::bufferType>(f.r);
                return *this;
            }
            FrameRef& operator+=(AudioFrame f) {
                port->bufL[at] += static_cast<Audio::bufferType>(f.l);
                port->bufR[at] += static_cast<Audio::bufferType>(f.r);
                return *this;
            }
            operator AudioFrame() const { return { port->bufL[at], port->bufR[at] }; }
            AudioFrame operator*(AudioFrame o) const { return static_cast<AudioFrame>(*this) * o; }
        };

        Audio::bufferType* bufL;
        Audio::bufferType* bufR;
        size_t size;

        AudioPort() = default;
        ~AudioPort() override = default;

        FrameRef operator[](size_t at) { return { this, at }; }
        FrameRef at(size_t at) { return { this, at }; } // alternative syntax if you don't like (*p)[0]

        static inline const constexpr Port::DataType DATA_TYPE = Port::Audio;
        Port::DataType dataType() const override { return DATA_TYPE; }

        void pull() override;
    };

    class CommandPort : public Port {
    public:
        uint8_t* data;
        size_t size;

        CommandPort() = default;
        ~CommandPort() override = default;

        static inline const constexpr Port::DataType DATA_TYPE = Port::Command;
        Port::DataType dataType() const override { return DATA_TYPE; }
        bool singleInput() const override { return true; }

        void pull() override;

        /// Push a data buffer.
        void push(const std::vector<uint8_t>&);
        /// Copy the data buffer of another port.
        void push(CommandPort*);
        /// Copy the data buffer of another port.
        inline void push(const std::shared_ptr<CommandPort>& o) { push(o.get()); }
    };

    class ParameterPort : public Port {
    public:
        double* data;
        size_t size;

        ParameterPort() = default;
        ~ParameterPort() override = default;

        double& operator[](size_t at) { return data[at]; }
        double& at(size_t at) { return data[at]; }

        static inline const constexpr Port::DataType DATA_TYPE = Port::Parameter;
        Port::DataType dataType() const override { return DATA_TYPE; }

        void pull() override;
    };
}
