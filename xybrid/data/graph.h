#pragma once

#include "data/node.h"

namespace Xybrid::Data {
    class Graph : public Node {
    public:
        Graph();
        ~Graph() override = default;

        std::vector<std::shared_ptr<Node>> children;

        // position of viewport within graph (not serialized)
        int viewX{}, viewY{};

        void reset() override;
        void release() override;
        void saveData(QCborMap&) const override;
        void loadData(const QCborMap&) override;

        //std::string pluginName() const override;
        void onParent(std::shared_ptr<Graph>) override;
        void onDoubleClick() override;
    };
}
