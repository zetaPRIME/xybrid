#pragma once

#include "data/audioframe.h"

#include <memory>

#include <array>
#include <vector>

#include <QString>
#include <QUuid>

class QCborValue;
class QCborMap;

namespace Xybrid::Data {
    class Project;
    class Sample : public std::enable_shared_from_this<Sample> {
    public:
        QUuid uuid;
        QString name;
        Project* project;
        //size_t index = static_cast<size_t>(-1);

        int sampleRate = 48000;

        std::array<std::vector<float>, 2> data;

        int loopStart = -1;
        int loopEnd = -1;

        int baseNote = 60;
        double subNote = 0.0;

        inline AudioFrame operator[] (size_t at) const {
            if (data[1].empty()) return {data[0][at]};
            return {data[0][at], data[1][at]};
        }

        inline int numChannels() const {
            if (data[0].empty()) return 0;
            if (data[1].empty()) return 1;
            return 2;
        }

        inline int length() const { return static_cast<int>(data[0].size()); }
        std::array<float, 2> plotBetween(size_t ch, size_t start, size_t end) const;

        inline double getNote() const { return static_cast<double>(baseNote) + subNote; }

        QCborMap toCbor() const;
        static std::shared_ptr<Sample> fromCbor(const QCborMap&, QUuid);
        static std::shared_ptr<Sample> fromCbor(const QCborValue&, QUuid);

        bool changeUuid(QUuid);
        void newUuid();

        static std::shared_ptr<Sample> fromFile(QString);

        static void startExport();
        static std::vector<std::shared_ptr<Sample>> finishExport();
        void markForExport();

    };
}
