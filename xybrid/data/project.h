#pragma once

#include <stdint.h>

#include <memory>
#include <vector>
#include <unordered_map>
#include <string>

#include <QString>
#include <QHash>

#include "data/pattern.h"
#include "data/sample.h"
#include "data/sequenceentry.h"

namespace Xybrid {
    class UISocket;
}

namespace Xybrid::Data {
    class Graph;
    class Project : public std::enable_shared_from_this<Project> {
    public:
        bool editingLocked();

        UISocket* socket;

        QString title;
        QString artist;
        QString comment;

        QString fileName;
        QString exportFileName;

        double tempo = 140.0;
        TimeSignature time;
        // default time signature

        // shared to ease reordering and prevent crashes due to invalidating things that UI stuff is using
        std::vector<std::shared_ptr<Pattern>> patterns;
        std::vector<SequenceEntry> sequence;

        std::shared_ptr<Graph> rootGraph;
        // list of input nodes is just part of rootGraph

        QHash<QUuid, std::shared_ptr<Sample>> samples;

        Project();
        ~Project();

        void updatePatternIndices();

        std::shared_ptr<Pattern> newPattern(size_t index = static_cast<size_t>(-1));

        void removePattern(Pattern*);
    };
}
