#include "audioframe.h"
using Xybrid::Data::AudioFrame;

#include <cmath>

namespace {
    const constexpr double PI = 3.141592653589793238462643383279502884197169399375105820974;
    const constexpr double PAN_MULT = 1.414213562373095048801688724209698078569671875376948073176;
}

AudioFrame AudioFrame::gainBalanceMult(double gain, double balance) {
    // calculate multipliers
    double gm = std::pow(10.0, gain / 20.0); // dBFS
    double s = (balance+1.0) * PI * 0.25;
    return {std::cos(s) * PAN_MULT * gm, std::sin(s) * PAN_MULT * gm};
}
