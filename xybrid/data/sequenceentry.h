#pragma once

#include <memory>

#include <QString>

class QCborValue;
namespace Xybrid::Data {
    class Project;
    class Pattern;
    struct SequenceEntry {
        enum Type : uint8_t {
            Separator,
            Pattern,

            LoopStart, LoopTrigger,
        };
        Type type = Separator;

        std::weak_ptr<Data::Pattern> p;

        inline SequenceEntry() = default;
        inline SequenceEntry(std::shared_ptr<Data::Pattern> p) : type(Pattern), p(p) { }
        inline SequenceEntry(Type t) : type(t) { }
        inline SequenceEntry(const SequenceEntry&) = default;
        inline SequenceEntry(SequenceEntry&&) = default;

        SequenceEntry& operator=(const SequenceEntry&) = default;

        // required for matching when deleting a pattern
        inline bool operator==(std::shared_ptr<Data::Pattern> o) const { return type == Pattern && p.lock() == o; }
        inline bool operator==(Data::Pattern* o) const { return type == Pattern && p.lock().get() == o; }

        SequenceEntry(Project*, const QCborValue&);
        operator QCborValue() const;

        inline std::shared_ptr<Data::Pattern> pattern() const {
            if (type == Pattern) return p.lock();
            return nullptr;
        }

        QString symbol() const;
        QString toolTip() const;
    };
}
