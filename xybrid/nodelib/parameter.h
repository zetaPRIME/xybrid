#pragma once

#include <cmath>
#include <limits>

#include <QCborMap>
#include <QStringList>

#include "data/node.h"
#include "data/porttypes.h"

#include "audio/audioengine.h"

#include "nodelib/basics.h"

namespace Xybrid::UI { class KnobGadget; }

//namespace Xybrid::Data { class ParameterPort; }
namespace Xybrid::NodeLib {
    /// Plugin parameter with associated metadata and automation faculties
    class Parameter : public Data::PortConfigurable {
        friend class Reader;
        //
        Parameter() = default;

        static inline double fb(double a, double b) { if (std::isnan(a)) return b; return a; }
        static inline double spow(double v, double e) { return std::pow(std::abs(v), e) * (v < 0 ? -1.0 : 1.0); }
        /*static inline const double base = 5;
        inline double curve(double in) { return 1.0 - std::cos(in * PI * 0.5); }//{ return (std::pow(base, in) - 1.0) / (base - 1.0); }//{ return spow(in, rExp); }//*/
        inline double curve(double in) { return spow(in, rExp); }
        inline double track(double in) {
            if (std::isnan(in)) return fb(vt, value);

            auto emax = fb(rMax, max);
            if (flags & SignedExtents) { // signed logic
                vt = std::clamp(curve(in) * emax, -emax, emax);
                return vt;
            }

            auto emin = fb(rMin, min);
            if (emin > emax) {
                std::swap(emin, emax);
                in = 1.0 - in;
            }
            vt = std::clamp(emin + curve(in) * (emax - emin), emin, emax);
            return vt;
        }
    public:
        class Reader {
            friend class Parameter;
            Parameter* p;
            size_t pos = 0;
            bool r;
            Reader(Parameter* p, bool r) : p(p), r(r) { }
        public:
            double next() {
                if (r) {
                    auto v = p->port->data[pos++];
                    return p->track(v);
                }
                return fb(p->vt, p->value);
            }
        };

        enum Flags : uint8_t {
            ResetOnTick = 1<<0,
            SignedExtents = 1<<1,
        };

        QString name;
        QString saveName;

        double min, max, def;

        double rMin = std::numeric_limits<double>::quiet_NaN();
        double rMax = std::numeric_limits<double>::quiet_NaN();
        double kStep = std::numeric_limits<double>::quiet_NaN();
        QPointer<UI::KnobGadget> knob;

        //double rShape = 0.0;
        double rExp = 1.0;

        double value;
        double vt = std::numeric_limits<double>::quiet_NaN();
        double lastVt = std::numeric_limits<double>::quiet_NaN();

        size_t tickId = -1;

        Flags flags;

        Data::ParameterPort* port = nullptr;

        Parameter(const QString& name, double min, double max, double def, Flags flags = { }) : Parameter() {
            this->name = name;
            this->min = min;
            this->max = max;
            this->def = def;
            this->value = def;
            this->flags = flags;

            if (min == -max) this->flags = static_cast<Flags>(this->flags | SignedExtents);

            saveName = name.toLower().remove(' ');
        }

        Data::ParameterPort* makePort(Data::Node*, const QString& name = { });
        void onGadgetCreated(UI::PortConfigObject*) override;
        QVariant getPortProperty(const QString&) override;

        inline operator double() const { return fb(vt, value); }

        /// Resets the *automation data* of the parameter.
        inline void reset() { vt = std::numeric_limits<double>::quiet_NaN(); lastVt = vt; }

        Reader start() {
            if (auto ct = Audio::audioEngine->curTickId(); ct != tickId) {
                tickId = ct;
                lastVt = vt;
            } else vt = lastVt; // reset state in case we're doing multiple readers per tick
            bool r = port && port->isConnected();
            if (r) port->pull();
            if (flags & ResetOnTick) vt = std::numeric_limits<double>::quiet_NaN();
            return Reader(this, r);
        }

        inline void save(QCborMap& m) const {
            m[saveName] = value;
            if (!std::isnan(rMin)) m[QStringLiteral("%1*rmin").arg(saveName)] = rMin;
            if (!std::isnan(rMax)) m[QStringLiteral("%1*rmax").arg(saveName)] = rMax;
        }

        inline void loadAs(const QCborMap& m, const QString& id) {
            auto dnan = std::numeric_limits<double>::quiet_NaN();

            value = m.value(id).toDouble(def);
            vt = dnan;
            lastVt = vt;

            rMin = m.value(QStringLiteral("%1*rmin").arg(id)).toDouble(dnan);
            rMax = m.value(QStringLiteral("%1*rmax").arg(id)).toDouble(dnan);
        }

        inline void load(const QCborMap& m) { loadAs(m, saveName); }
        inline void load(const QCborMap& m, const QStringList& ids) {
            value = def;
            vt = std::numeric_limits<double>::quiet_NaN();
            lastVt = vt;
            if (auto v = m.value(saveName); v.isDouble())
                loadAs(m, saveName);
            else {
                for (auto& id : ids) {
                    if (auto v = m.value(id); v.isDouble()) { loadAs(m, id); break; }
                }
            }
        }
    };
}
