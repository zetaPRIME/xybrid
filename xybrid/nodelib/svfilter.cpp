#include "svfilter.h"

#include "nodelib/basics.h"
#include "audio/audioengine.h"

using Xybrid::NodeLib::SVFilter;
using Xybrid::NodeLib::GenericSVFilter;
using Xybrid::Data::AudioFrame;
using namespace Xybrid::Audio;

template class Xybrid::NodeLib::GenericSVFilter<AudioFrame>;
template class Xybrid::NodeLib::GenericSVFilter<double>;
