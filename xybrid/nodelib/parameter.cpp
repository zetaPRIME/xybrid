#include "parameter.h"

#include "ui/patchboard/nodeobject.h"

#include "ui/gadgets/layoutgadget.h"
#include "ui/gadgets/knobgadget.h"
#include "ui/gadgets/labelgadget.h"

using Xybrid::NodeLib::Parameter;
using namespace Xybrid::Data;
using namespace Xybrid::UI;

ParameterPort* Parameter::makePort(Data::Node* node, const QString& name) {
    if (port) return port;

    node->inputs.try_emplace(Port::Parameter);
    auto& inp = node->inputs.find(Port::Parameter)->second;

    uint8_t id = 0;
    auto it = inp.begin();
    while (it != inp.end() && it->first == id) { ++id; ++it; } // scan for first unused id

    auto n = name;
    if (n.isEmpty()) n = this->name.toLower();

    port = static_cast<ParameterPort*>(node->addPort(Port::Input, Port::Parameter, id).get());
    port->name = n;
    port->configurable = this;
    return port;
}

void Parameter::onGadgetCreated(PortConfigObject* obj) {
    auto dnan = std::numeric_limits<double>::quiet_NaN();
    auto l = new LayoutGadget(obj);
    auto l2 = new LayoutGadget(l, true);
    auto l3 = new LayoutGadget(l2);
    (new LabelGadget(l2))->setText(qs("Automation"));

    if (flags & SignedExtents) {
        auto kmax = KnobGadget::autoAlike(l3, knob)->setDefault(dnan)->setLabel(qs("Magnitude"));
        kmax->fSet = [this](double v) { rMax = v; };
        kmax->fGet = [this]() { if (std::isnan(rMax)) return max; return rMax; };
    } else {
        auto kmin = KnobGadget::autoAlike(l3, knob)->setDefault(dnan)->setLabel(qs("Min"));
        kmin->fSet = [this](double v) { rMin = v; };
        kmin->fGet = [this]() { if (std::isnan(rMin)) return min; return rMin; };

        auto kmax = KnobGadget::autoAlike(l3, knob)->setDefault(dnan)->setLabel(qs("Max"));
        kmax->fSet = [this](double v) { rMax = v; };
        kmax->fGet = [this]() { if (std::isnan(rMax)) return max; return rMax; };
    }
}

QVariant Parameter::getPortProperty(const QString& id) {
    if (id == "signed") return flags & SignedExtents;
    return { };
}
