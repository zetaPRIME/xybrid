#include "basics.h"
using namespace Xybrid::NodeLib;

#include <cmath>

#include <QCborMap>
#include <QCborValue>
#include <QCborArray>

ADSR ADSR::normalized() {
    ADSR adsr = *this;
    adsr.a = std::max(adsr.a, shortStep);
    adsr.r = std::max(adsr.r, shortStep);
    if (adsr.s != 1.0) adsr.d = std::max(adsr.d, shortStep);
    return adsr;
}

ADSR::ADSR(const QCborMap& m) {
    a = m.value("a").toDouble(a);
    d = m.value("d").toDouble(d);
    s = m.value("s").toDouble(s);
    r = m.value("r").toDouble(r);
}
ADSR::ADSR(const QCborValue& v) : ADSR(v.toMap()) { }

ADSR::operator QCborMap() const {
    QCborMap m;
    m.insert(QString("a"), a);
    m.insert(QString("d"), d);
    m.insert(QString("s"), s);
    m.insert(QString("r"), r);
    return m;
}
ADSR::operator QCborValue() const { return QCborMap(*this).toCborValue(); }
