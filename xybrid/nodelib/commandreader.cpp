#include "commandreader.h"
using namespace Xybrid::NodeLib;
using namespace Xybrid::Data;

#include "data/porttypes.h"


CommandReader::CommandReader(CommandPort* p) {
    p->pull();
    data = p->data;
    dataSize = p->size;
}

CommandReader::operator bool() const { return dataSize >= cur+5; }
CommandReader& CommandReader::operator++() {
    if (cur == static_cast<size_t>(-1)) cur = 0;
    else if (dataSize >= cur+5) cur += 5 + data[cur+4]*2;
    return *this;
}

ParamReader::ParamReader(const CommandReader& cr) : cr(cr) {
    if (cr) {
        pmax = cr.numParams();
    }
}

ParamReader::operator bool() const { return pn >= 0 && pn < pmax; }
ParamReader &ParamReader::operator++() {
    ++pn;
    return *this;
}

int16_t ParamReader::next(bool acceptsTweens, uint8_t num) const {
    auto n = static_cast<uint8_t>(pn);
    while (++n < pmax) {
        auto p = cr.param(n);
        if (p != ',' && (acceptsTweens && p != 't')) break;
        if (p == ',' && --num == 0) return cr.val(n);
    }
    return -1;
}

int16_t ParamReader::tween(bool twoByte) const {
    auto n = static_cast<uint8_t>(pn);
    while (++n < pmax) {
        auto p = cr.param(n);
        if (p == 't') {
            int16_t v = cr.val(n);
            if (twoByte && n+1 < pmax && cr.param(n+1) == ',') v += std::min(static_cast<int16_t>(cr.val(n+1)), static_cast<int16_t>(127)) * 256;
            return v;
        }
        if (p == ',') continue;
        break;
    }
    return -1;
}

int ParamReader::tweenInt() const {
    auto n = static_cast<uint8_t>(pn);
    while (++n < pmax) {
        auto p = cr.param(n);
        if (p == 't') {
            int v = cr.val(n);
            if (n+1 < pmax && cr.param(n+1) == ',') v += static_cast<int>(cr.val(n+1)) * 256;
            return v;
        }
        if (p == ',') continue;
        break;
    }
    return -1;
}

