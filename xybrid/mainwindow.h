#pragma once

#include <unordered_set>

#include <QMainWindow>

#include "uisocket.h"
#include "data/project.h"
#include "ui/patterneditormodel.h"

class QUndoStack;

namespace Ui {
    class MainWindow;
}

namespace Xybrid {
    namespace Data { class Node; }
    class MainWindow : public QMainWindow {
        friend class Data::Graph;
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr, const QString& fileName = QString());
        ~MainWindow() override;

        static std::unordered_set<MainWindow*> openWindows;
        static MainWindow* projectWindow(const QString& fileName);

    private:
        Ui::MainWindow* ui;
        UISocket* socket;
        std::shared_ptr<Data::Project> project;
        std::shared_ptr<Data::Pattern> editingPattern;
        std::shared_ptr<Data::Sample> editingSample;

        QUndoStack* undoStack;
        std::vector<QAction*> recentFileActions;

        void openProject(const QString& fileName, bool failSilent = false);
        void openRecentProject(size_t idx);
        bool promptSave();

        void onNewProjectLoaded();
        void updatePatternLists();
        bool selectPatternForEditing(Data::Pattern*);

        void selectSampleForEditing(std::shared_ptr<Data::Sample>);

        void openGraph(const std::shared_ptr<Data::Graph>&);
        void openNodeUI(const std::shared_ptr<Data::Node>&);

        void openPatternProperties(const std::shared_ptr<Data::Pattern>&);

        void updateTitle();
        void updateFont();

        void setSongInfoPaneExpanded(bool);
        void setFloater(QWidget* = nullptr);

        void render();

    public:
        const std::shared_ptr<Data::Project>& getProject() const { return project; }

        int patternSelection(int = -100);
        int sequenceSelection(int = -100);

        void playbackPosition(int seq, int row);

        inline UISocket* uiSocket() { return socket; }

    protected:
        void resizeEvent(QResizeEvent*) override;
        void closeEvent(QCloseEvent*) override;
        bool eventFilter(QObject *obj, QEvent *event) override;

    public slots:
        void tryFocus();

        void menuFileNew();
        void menuFileOpen();
        void menuFileSave();
        void menuFileSaveAs();

        void menuFileExport();
        void menuFileExportAs();

        void menuFileNewWindow();
        void menuSettings();
        void menuAbout();
        void menuQuit();

    signals:
        void projectLoaded();

    };
}
