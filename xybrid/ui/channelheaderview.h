#pragma once

#include <QHeaderView>
#include <QStyledItemDelegate>

namespace Xybrid::UI {
    class ChannelHeaderView : public QHeaderView {
    public:
        ChannelHeaderView(QWidget* parent = nullptr);
    };

    class ChannelHeaderItemDelegate : public QStyledItemDelegate {
        Q_OBJECT
    public:
        ChannelHeaderItemDelegate(QWidget *parent = nullptr) : QStyledItemDelegate(parent) {}

        //bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;

    protected:
        //QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
        //void setEditorData(QWidget *editor, const QModelIndex &index) const override;
        //void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    };
}
