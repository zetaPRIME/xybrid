#pragma once

#include <functional>

#include <QString>
#include <QVector>
#include <QVariant>

namespace Xybrid::UI {
    class DirectoryNode {
        //

    public:
        QString name;
        QVariant data;

        DirectoryNode* parent;
        QVector<DirectoryNode*> children;
        int index = 0;

        DirectoryNode(DirectoryNode* parent = nullptr, QString name = QString(), QVariant data = QVariant());
        ~DirectoryNode();

        void clear();

        void sortChildren();
        void sortTree();

        DirectoryNode* subdir(QString name);

        DirectoryNode* placeData(QString name, QVariant data);
        //void placeData(const QVector<std::pair<QString, QVariant>>&);

        QString path() const;
        DirectoryNode* findPath(QString path);
        DirectoryNode* findData(const QVariant&);
        bool isChildOf(DirectoryNode*) const;

        void treeExec(const std::function<void(DirectoryNode*)>&);

        inline bool isDirectory() const { return data.isNull(); }
    };
}
