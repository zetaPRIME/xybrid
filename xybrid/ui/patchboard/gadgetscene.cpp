#include "gadgetscene.h"
using Xybrid::UI::GadgetScene;

#include <QKeyEvent>

#include "ui/gadgets/labelgadget.h"

GadgetScene::GadgetScene(QGraphicsView* view) : QGraphicsScene(view) {
    this->view = view;
}

void GadgetScene::toolTip(QGraphicsItem* g, const QString& s, const QPointF& pos, const QColor& color) {
    if (!g) return; // umm???
    if (s.isEmpty()) {
        if (toolTipSource && toolTipSource != g) return;
        // remove existing tool tip
        if (toolTipObject) {
            toolTipObject->setVisible(false); // hide immediately to ensure ghosting doesn't happen
            toolTipObject->deleteLater();
        }
        toolTipObject = nullptr;
    } else {
        // create/set up
        toolTipSource = g;
        toolTipText = s;
        if (!toolTipObject) {
            toolTipObject = new LabelGadget();
            addItem(toolTipObject);
            toolTipObject->setZValue(10000);
        }
        auto r = g->boundingRect().translated(g->scenePos());
        toolTipObject->setText(s)->setColor(color)->useOutline(true);

        constexpr double pad = 2.0;
        auto tr = toolTipObject->boundingRect();
        QPointF f = {(r.width() + tr.width()) / 2.0 + pad, (r.height() + tr.height()) / 2.0 + pad};

        toolTipObject->centerOn(r.center() + QPointF(pos.x() * f.x(), pos.y() * f.y()));
    }

}

void GadgetScene::keyPressEvent(QKeyEvent* e) {
    QGraphicsScene::keyPressEvent(e);
    if (!e->isAccepted() && !e->isAutoRepeat()) {
        auto key = e->key();
        if (key == Qt::Key_Escape) {
            emit onKeyEscape();
            e->accept();
            return;
        }
    }
}
