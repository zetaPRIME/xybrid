#pragma once

#include <QGraphicsScene>
#include <QGraphicsView>

namespace Xybrid::UI {
    class LabelGadget;
    class GadgetScene : public QGraphicsScene {
        Q_OBJECT

        LabelGadget* toolTipObject = nullptr;
        QGraphicsItem* toolTipSource = nullptr;
        QString toolTipText;

    protected:
        QGraphicsView* view;

    public:
        GadgetScene(QGraphicsView* view);
        ~GadgetScene() override = default;

        void toolTip(QGraphicsItem*, const QString& = { }, const QPointF& = {0, 0}, const QColor& = {255, 255, 255});

        void keyPressEvent(QKeyEvent*) override;

    signals:
        void onKeyEscape();
    };
}
