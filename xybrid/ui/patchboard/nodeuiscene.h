#pragma once

#include "ui/patchboard/gadgetscene.h"

#include <memory>
#include <unordered_map>

class QShortcut;

namespace Xybrid::Data { class Node; }

namespace Xybrid::UI {
    class NodeUIScene : public GadgetScene {
        Q_OBJECT

        std::shared_ptr<Data::Node> node;

        bool resizeQueued = false;

        std::unordered_map<int, std::pair<int16_t, uint16_t>> previewKey;

        void startPreview(int, int16_t);
        void stopPreview(int);

        //QShortcut* shortcut(QKeySequence);

        std::shared_ptr<void> stateObject;

    public:
        bool autoCenter = true;

        NodeUIScene(QGraphicsView* view, const std::shared_ptr<Data::Node>& node);
        ~NodeUIScene() override;// = default;

        void queueResize();
        void autoResize();

        void drawBackground(QPainter*, const QRectF&) override;

        //void contextMenuEvent(QGraphicsSceneContextMenuEvent*) override;
        void keyPressEvent(QKeyEvent*) override;
        void keyReleaseEvent(QKeyEvent*) override;

        // default constructs an object of given type and manages its lifetime
        template<typename T> inline T* makeStateObject() { auto o = std::make_shared<T>(); stateObject = o; return o.get(); }

    signals:
        void finalized();
        void notePreview(int16_t);

    };
}
