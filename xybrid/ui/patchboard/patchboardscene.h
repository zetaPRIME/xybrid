#pragma once

#include "ui/patchboard/gadgetscene.h"

#include <memory>
#include <unordered_map>

class QShortcut;

namespace Xybrid::Data { class Graph; }

namespace Xybrid::UI {
    class PatchboardScene : public GadgetScene {
        std::shared_ptr<Data::Graph> graph;

        std::unordered_map<int, std::pair<int16_t, uint16_t>> previewKey;

        bool resizeQueued = false;
        void queueResize();
        void autoResize();

        void startPreview(int, int16_t);
        void stopPreview(int);

        QShortcut* shortcut(QKeySequence);

    public:
        PatchboardScene(QGraphicsView* view, const std::shared_ptr<Data::Graph>& graph);
        ~PatchboardScene() override = default;

        void drawBackground(QPainter*, const QRectF&) override;

        void contextMenuEvent(QGraphicsSceneContextMenuEvent*) override;
        void keyPressEvent(QKeyEvent*) override;
        void keyReleaseEvent(QKeyEvent*) override;

        void refresh();
    };
}
