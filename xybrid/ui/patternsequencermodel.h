#pragma once

#include <QAbstractTableModel>

#include "data/project.h"

namespace Xybrid {
    class MainWindow;
}

namespace Xybrid::UI {
    class PatternSequencerModel : public QAbstractTableModel {
        Q_OBJECT

        // raw pointer because the model's lifetime is dependent on the window
        MainWindow* window;
    public:
        PatternSequencerModel(QObject* parent, MainWindow* window);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;
        int columnCount(const QModelIndex &parent = QModelIndex()) const override;
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
        Qt::ItemFlags flags(const QModelIndex & index) const override;

        Qt::DropActions supportedDropActions() const override;
        QStringList mimeTypes() const override;
        QMimeData* mimeData(const QModelIndexList &indexes) const override;
        bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
    };
}
