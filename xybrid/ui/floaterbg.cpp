#include "floaterbg.h"

#include <QVariant>

using Xybrid::UI::FloaterBG;

FloaterBG::FloaterBG(QWidget* parent) : QWidget(parent) {
    // set custom appearance
    setStyleSheet(R"css(
        background: palette(window);
        border: 1px solid palette(mid);
        border-radius: 5;
    )css");
}
