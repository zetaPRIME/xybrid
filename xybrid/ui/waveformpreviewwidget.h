#pragma once

#include <memory>

#include <QFrame>

namespace Xybrid::Data { class Sample; }
namespace Xybrid::UI {
    class WaveformPreviewWidget : public QFrame {
        Q_OBJECT

        std::weak_ptr<Data::Sample> currentSample;
        bool highlighted = false;
    public:
        bool showName = false;
        bool highlightable = false;
        bool showLoopPoints = true;

        explicit WaveformPreviewWidget(QWidget* parent = nullptr);

        void setSample(std::shared_ptr<Data::Sample>);

    protected:
        void paintEvent(QPaintEvent* event) override;
        void paintChannel(QPainter&, std::shared_ptr<Data::Sample>, size_t channel, QRect);

        void enterEvent(QEvent*) override;
        void leaveEvent(QEvent*) override;
    };
}
