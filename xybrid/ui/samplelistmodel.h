#pragma once

#include "ui/directorynode.h"

#include <QAbstractItemModel>

#include "data/project.h"

namespace Xybrid {
    class MainWindow;
}
namespace Xybrid::Data {
    class Sample;
}

namespace Xybrid::UI {
    class SampleListModel : public QAbstractItemModel {
        Q_OBJECT

        // raw pointer because the model's lifetime is dependent on the window
        MainWindow* window;

        std::shared_ptr<DirectoryNode> root;
        std::shared_ptr<DirectoryNode> hold;

        void propagateSampleNames(DirectoryNode*);
    public:
        SampleListModel(QObject* parent, MainWindow* window);

        std::shared_ptr<Data::Sample> itemAt(const QModelIndex&);
        void refresh();

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;
        int columnCount(const QModelIndex &parent = QModelIndex()) const override;
        bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
        Qt::ItemFlags flags(const QModelIndex& index) const override;
        QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
        QModelIndex parent(const QModelIndex& index) const override;
        QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

        Qt::DropActions supportedDropActions() const override;
        QStringList mimeTypes() const override;
        QMimeData* mimeData(const QModelIndexList &indexes) const override;
        bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
        bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
    };
}
