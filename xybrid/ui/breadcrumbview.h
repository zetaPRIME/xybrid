#pragma once

#include <memory>
#include <functional>
#include <vector>

#include <QAction>
#include <QTableView>
#include <QAbstractTableModel>

namespace Xybrid::UI {
    class BreadcrumbView;
    class BreadcrumbModel : public QAbstractTableModel {
        friend class BreadcrumbView;
        Q_OBJECT

        std::vector<std::unique_ptr<QAction>> actions;

        BreadcrumbModel(QWidget* parent = nullptr);
    public:

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;
        int columnCount(const QModelIndex &parent = QModelIndex()) const override;
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    };

    class BreadcrumbView : public QTableView {
        Q_OBJECT
        BreadcrumbModel* mdl;

    public:
        bool shrinkOnSelect = true;

        BreadcrumbView(QWidget* parent = nullptr);
        void clear();
        void push(const QString& text, QObject* bind, std::function<void()> action);

        void up();
    };
}
