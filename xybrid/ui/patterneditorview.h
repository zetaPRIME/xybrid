#pragma once

#include <memory>
#include <unordered_map>

#include <QCheckBox>
#include <QTableView>

#include "data/pattern.h"
#include "ui/channelheaderview.h"
/*#include "ui/patterneditormodel.h"
#include "ui/patterneditoritemdelegate.h"*/

namespace Xybrid::UI {
    class PatternEditorModel;
    class PatternEditorItemDelegate;
    class PatternEditorView : public QTableView {
        Q_OBJECT

        std::unique_ptr<PatternEditorModel> mdl;
        std::unique_ptr<PatternEditorItemDelegate> del;
        std::unique_ptr<ChannelHeaderView> hdr;
        std::unique_ptr<QWidget> cornerBoxBox;
        std::unique_ptr<QCheckBox> cornerBox;

        std::unordered_map<int, std::pair<int16_t, uint16_t>> previewKey;

        bool colUpdateNeeded = false;

    public:
        PatternEditorView(QWidget* parent = nullptr);
        ~PatternEditorView() override;

        void updateGeometries() override;
        void keyPressEvent(QKeyEvent*) override;
        void keyReleaseEvent(QKeyEvent*) override;
        void keyboardSearch(const QString&) override {} // disable accidental search

        void mouseReleaseEvent(QMouseEvent*) override;

        void setPattern(const std::shared_ptr<Data::Pattern>& pattern);

        void updateHeader(bool full = false);

        void refresh(int level);

        bool isFolded();

    private:
        void startPreview(int);
        void stopPreview(int);

        void updateHeaderOffset(int);
        void headerMoved(int logicalIndex, int oldVisualIndex, int newVisualIndex);
        void headerDoubleClicked(int section);
        void headerContextMenu(QPoint pt);

        void startRenameChannel(int channel);

    };
}
