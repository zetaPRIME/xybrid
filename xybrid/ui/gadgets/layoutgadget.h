#pragma once

#include "ui/gadgets/gadget.h"

namespace Xybrid::UI {
    class NodeObject;
    class PortConfigObject;
    class NodeUIScene;
    class LayoutGadget : public Gadget {
        QSizeF size;

        QPointF orient(const QPointF&) const;
        QSizeF orient(const QSizeF&) const;
        QRectF orient(const QRectF&) const;

    public:
        QSizeF minSize = {0, 0};

        qreal margin = 6;
        qreal spacing = 6;
        qreal bias = 0.5;

        bool vertical = false;
        bool drawPanel = false;
        qreal panelMargin = 6;

        LayoutGadget(QGraphicsItem* parent = nullptr, bool vertical = false);
        LayoutGadget(NodeObject* parent, bool vertical = false);
        LayoutGadget(PortConfigObject* parent, bool vertical = false);
        LayoutGadget(NodeUIScene* parent, bool vertical = false);
        ~LayoutGadget() override = default;

        inline LayoutGadget* setMetrics(qreal margin = -1, qreal spacing = -1, qreal bias = -1) {
            if (margin >= 0) this->margin = margin;
            if (spacing >= 0) this->spacing = spacing;
            if (bias >= 0) this->bias = std::clamp(bias, 0.0, 1.0);
            return this;
        }
        inline LayoutGadget* setPanel(bool draw, qreal margin = 6) {
            drawPanel = draw;
            panelMargin = margin;
            return this;
        }

        LayoutGadget* addSpacer();

        void updateGeometry() override;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    };
}
