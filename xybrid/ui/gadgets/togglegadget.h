#pragma once

#include "ui/gadgets/gadget.h"

#include <memory>
#include <functional>

#include <QFont>

namespace Xybrid::UI {
    class ToggleGadget : public Gadget {

    private:
        QFont font;
        bool pressed = false;

        //QGraphicsSimpleTextItem* labelShadow = nullptr;
        //QGraphicsSimpleTextItem* label = nullptr;

    public:
        QString text;
        QColor color = {127, 127, 127};
        QPointF toolTipPosition = {0, -1};

        std::function<bool()> fGet = [] { return false; };
        std::function<void(bool)> fSet = [](bool) { };

        ToggleGadget(QGraphicsItem* parent = nullptr);
        ~ToggleGadget() override = default;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

        void mousePressEvent(QGraphicsSceneMouseEvent*) override;
        void mouseReleaseEvent(QGraphicsSceneMouseEvent*) override;

        void hoverEnterEvent(QGraphicsSceneHoverEvent*) override;
        void hoverLeaveEvent(QGraphicsSceneHoverEvent*) override;

        ToggleGadget* bind(bool&);
        inline ToggleGadget* setColor(const QColor& c) { color = c; update(); return this; }
        inline ToggleGadget* setToolTip(const QString& t, const QPointF pos = {}) {
            text = t;
            if (!pos.isNull()) toolTipPosition = pos;
            update();
            return this;
        }

    };
}
