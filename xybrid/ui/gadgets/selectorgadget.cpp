#include "selectorgadget.h"
using Xybrid::UI::SelectorGadget;

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QCursor>

#include <QMenu>

#include "config/uiconfig.h"
#include "theme/patchboardstyle.h"
using Xybrid::Config::UIConfig::patchboardStyle;

SelectorGadget::SelectorGadget(QGraphicsItem *parent) : Gadget(parent) {
    setAcceptHoverEvents(true);

    font = QFont("Arcon Rounded", 8);
}

QRectF SelectorGadget::boundingRect() const {
    return QRectF(QPointF(0, 0), QSizeF(width, 24));
}

void SelectorGadget::paint(QPainter* p, const QStyleOptionGraphicsItem* opt, QWidget*) {
    auto r = boundingRect();
    qreal tw = 6;

    patchboardStyle->drawSelectorBacking(p, opt, r);

    p->setFont(font);
    p->setBrush(QBrush(Qt::NoBrush));
    p->setPen(QColor(255, 255, 255));
    QFontMetrics f(font);
    p->drawText(QPointF(r.left() + 4, r.center().y() + f.ascent()/2), f.elidedText(_entry.second, Qt::ElideRight, static_cast<int>(r.width() - 8-2-tw)));
}

void SelectorGadget::mousePressEvent(QGraphicsSceneMouseEvent* e) {
    if (e->button() == Qt::LeftButton) {
        auto* m = new QMenu();
        std::vector<Entry> v = fGetList ? fGetList() : std::vector<Entry>();
        if (v.empty()) m->addAction("(no entries)")->setDisabled(true);
        else {
            for (auto& e : v) {
                if (_entry.first == e.first) {
                    m->addAction(e.second)->setDisabled(true);
                } else m->addAction(e.second, this, [this, e] {
                    setEntry(e);
                });
            }
        }
        if (fEditMenu) fEditMenu(m);
        m->setAttribute(Qt::WA_DeleteOnClose);
        m->popup(e->screenPos());
    }
}

void SelectorGadget::hoverEnterEvent(QGraphicsSceneHoverEvent*) { update(); }
void SelectorGadget::hoverLeaveEvent(QGraphicsSceneHoverEvent*) { update(); }
