#include "gadget.h"
using Xybrid::UI::Gadget;

#include "ui/patchboard/gadgetscene.h"

Gadget::Gadget(QGraphicsItem* parent) : QGraphicsObject(parent) { }

void Gadget::centerOn(const QPointF& newPos) {
    auto c = boundingRect().center();
    setPos(newPos - c);
    // check and compensate
    if (auto nc = pos() + c; nc != newPos) setPos(newPos - (nc - newPos));
}

void Gadget::toolTip(const QString& s, const QPointF& pos, const QColor& color) {
    auto gs = static_cast<GadgetScene*>(scene());
    gs->toolTip(this, s, pos, color);
}

void Gadget::updateGeometry() { }

QRectF Gadget::boundingRect() const { return { }; }

QRectF Gadget::layoutBoundingRect() const { return boundingRect(); }

void Gadget::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) { }
