#include "labelgadget.h"
using Xybrid::UI::LabelGadget;

#include <QFont>
#include <QFontMetricsF>

#include <QPainter>

namespace { // clazy:excludeall=non-pod-global-static
    const QFont font("Arcon Rounded", 8);
}

LabelGadget::LabelGadget(QGraphicsItem* parent, const QString& t) : Gadget(parent) {
    setText(t);
}

QRectF LabelGadget::boundingRect() const {
    return {{ }, size};
}

void LabelGadget::paint(QPainter* p, const QStyleOptionGraphicsItem*, QWidget*) {
    if (text.isEmpty()) return;
    //p->fillRect(boundingRect(), {255, 0, 255, 127});
    QFontMetricsF fm(font);
    QPainterPath path;
    path.addText({1, fm.ascent() + 1}, font, text);

    if (outlineEnabled) {
        auto oc = color.lighter(static_cast<int>(color.saturationF() * 20));
        p->fillPath(path, oc);
        p->strokePath(path, QPen(oc, 3));
    }
    p->fillPath(path, color);
}

LabelGadget* LabelGadget::setText(const QString& t) {
    text = t;
    QFontMetricsF fm(font);
    size = {fm.horizontalAdvance(text) + 2, fm.height() + 2};
    update();
    return this;
}
