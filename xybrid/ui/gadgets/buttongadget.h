#pragma once

#include "ui/gadgets/gadget.h"

#include <memory>
#include <functional>

#include <QFont>

namespace Xybrid::UI {
    class ButtonGadget : public Gadget {
        Q_OBJECT

    private:
        QSizeF size = {96, 24};
        bool pressed = false;

    public:
        QString text;

        ButtonGadget(QGraphicsItem* parent = nullptr);
        ~ButtonGadget() override = default;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

        void mousePressEvent(QGraphicsSceneMouseEvent*) override;
        void mouseReleaseEvent(QGraphicsSceneMouseEvent*) override;

        void hoverEnterEvent(QGraphicsSceneHoverEvent*) override;
        void hoverLeaveEvent(QGraphicsSceneHoverEvent*) override;

        inline ButtonGadget* setSize(QSizeF s) { size = s; update(); return this; }
        inline ButtonGadget* setSize(qreal w, qreal h = 24) { size = {w, h}; update(); return this; }
        inline ButtonGadget* setText(const QString& t) { text = t; update(); return this;}

        template<typename F> ButtonGadget* onClick(F f) { connect(this, &ButtonGadget::clicked, f); return this; }

    signals:
        void clicked();
    };
}
