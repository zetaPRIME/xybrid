#pragma once

#include "ui/gadgets/gadget.h"

namespace Xybrid::UI {
    class LabelGadget : public Gadget {
        QSizeF size;

        QString text;
        QColor color = {255, 255, 255};
        bool outlineEnabled = false;

    public:

        LabelGadget(QGraphicsItem* parent = nullptr, const QString& = { });
        ~LabelGadget() override = default;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

        LabelGadget* setText(const QString&);
        inline LabelGadget* setColor(const QColor& c) { color = c; update(); return this; }
        inline LabelGadget* useOutline(bool b = true) { outlineEnabled = b; update(); return this;}
    };
}
