#include "paramselectorgadget.h"
using Xybrid::UI::ParamSelectorGadget;

#include <cmath>

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QCursor>

#include <QMenu>

#include <QKeyEvent>

#include "config/colorscheme.h"
using Xybrid::Config::colorScheme;

#include "config/uiconfig.h"
#include "theme/patchboardstyle.h"
using Xybrid::Config::UIConfig::patchboardStyle;

#include "util/strings.h"
#include "util/keys.h"

ParamSelectorGadget::ParamSelectorGadget(QGraphicsItem *parent) : Gadget(parent) {
    setAcceptHoverEvents(true);
    setFlag(GraphicsItemFlag::ItemIsFocusable);

    font = QFont("Iosevka Term Light", 10);
}

QRectF ParamSelectorGadget::boundingRect() const {
    return QRectF(QPointF(0, 0), QSizeF(32, 24));
}

void ParamSelectorGadget::paint(QPainter* p, const QStyleOptionGraphicsItem* opt, QWidget*) {
    auto r = boundingRect();
    patchboardStyle->drawPatternSelectorBacking(p, opt, r);

    p->setFont(font);
    p->setBrush(QBrush(Qt::NoBrush));
    QFontMetrics f(font);
    char ch = ' ';
    if (fGet) ch = fGet();
    if (ch == ' ') {
        QString text = qs(" - ");
        QPointF tp(r.center().x() - f.horizontalAdvance(text) / 2, r.center().y() + f.ascent()/2);
        p->setPen(colorScheme.patternFgBlank);
        p->drawText(tp, text);
    } else {
        QString text = qs("%1  ").arg(ch);
        QPointF tp(r.center().x() - f.horizontalAdvance(text) / 2, r.center().y() + f.ascent()/2);
        p->setPen(colorScheme.patternFgParamCmd);
        p->drawText(tp, text);
        p->setPen(colorScheme.patternFgParamAmt);
        p->drawText(tp, qs(" XX"));
    }
}

void ParamSelectorGadget::mousePressEvent(QGraphicsSceneMouseEvent* e) {
    if (e->button() == Qt::LeftButton) {
        setFocus(Qt::MouseFocusReason);
        update();
    }
}

void ParamSelectorGadget::hoverEnterEvent(QGraphicsSceneHoverEvent*) { update(); }
void ParamSelectorGadget::hoverLeaveEvent(QGraphicsSceneHoverEvent*) { update(); }

void ParamSelectorGadget::keyPressEvent(QKeyEvent* e) {
    auto key = e->key();
    if (Util::isModifier(key)) { // we need to be able to press shift here
        e->accept();
        return;
    } else if (key == Qt::Key_Escape || key == Qt::Key_Enter) { // accept the key and still exit focus
        e->accept();
        clearFocus();
        return;
    }
    auto txt = e->text();
    if (!txt.isEmpty() && txt.length() == 1) {
        char chr = txt.toUtf8()[0];
        if (key == Qt::Key_Delete || key == Qt::Key_Backspace) chr = ' ';
        if (fSet) fSet(chr);
        e->accept();
    }
    clearFocus();
    update();
}
