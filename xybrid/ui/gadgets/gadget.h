#pragma once

#include <QGraphicsObject>

namespace Xybrid::UI {
    class Gadget : public QGraphicsObject {
        Q_OBJECT
    public:
        Gadget(QGraphicsItem* parent = nullptr);
        ~Gadget() override = default;

        void centerOn(const QPointF&);
        inline void centerOn(qreal x, qreal y) { centerOn(QPointF(x, y)); }

        void toolTip(const QString& = { }, const QPointF& = {0, 0}, const QColor& = {255, 255, 255});

        virtual void updateGeometry();

        QRectF boundingRect() const override;
        virtual QRectF layoutBoundingRect() const;

        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    };
}
