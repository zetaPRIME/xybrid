#pragma once

#include "ui/gadgets/gadget.h"

#include <memory>
#include <functional>

#include <QFont>

namespace Xybrid::UI {
    class ParamSelectorGadget : public Gadget {
        Q_OBJECT

        QFont font;

    public:
        std::function<char()> fGet;
        std::function<void(char)> fSet;

        ParamSelectorGadget(QGraphicsItem* parent = nullptr);
        ~ParamSelectorGadget() override = default;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

        void mousePressEvent(QGraphicsSceneMouseEvent*) override;

        void hoverEnterEvent(QGraphicsSceneHoverEvent*) override;
        void hoverLeaveEvent(QGraphicsSceneHoverEvent*) override;

        void keyPressEvent(QKeyEvent*) override;

        ParamSelectorGadget* bind(char& c) {
            auto p = &c;
            fGet = [p] { return *p; };
            fSet = [p](char v) { *p = v; };
            update();
            return this;
        }

        //

    signals:
        //void onSelect(const Xybrid::UI::SelectorGadget::Entry&);
    };
}
