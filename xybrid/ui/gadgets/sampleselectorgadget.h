#pragma once
#include "ui/gadgets/gadget.h"

#include <memory>

namespace Xybrid::Data { class Project; class Sample; }
namespace Xybrid::UI {
    class DirectoryNode;
    class SampleSelectorGadget : public Gadget {
        Q_OBJECT

        QSizeF size = {128, 32};
        Data::Project* project;
        std::weak_ptr<Data::Sample> currentSample;

        void buildSubmenu(DirectoryNode* dir, QMenu* menu);

    public:
        SampleSelectorGadget(Data::Project* project = nullptr, QGraphicsItem* parent = nullptr);
        ~SampleSelectorGadget() override = default;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

        void mousePressEvent(QGraphicsSceneMouseEvent*) override;

        void hoverEnterEvent(QGraphicsSceneHoverEvent*) override;
        void hoverLeaveEvent(QGraphicsSceneHoverEvent*) override;

        inline SampleSelectorGadget* setSize(const QSizeF& s) { size = s; update(); return this; }
        inline SampleSelectorGadget* setSize(qreal w, qreal h) { return setSize({w, h}); }

        inline SampleSelectorGadget* setSample(std::shared_ptr<Data::Sample> smp, bool signal = true) {
            currentSample = smp;
            update();
            if (signal) emit sampleSelected(smp);
            return this;
        }

    signals:
        void sampleSelected(std::shared_ptr<Data::Sample>);
    };
}
