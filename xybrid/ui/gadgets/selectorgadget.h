#pragma once

#include "ui/gadgets/gadget.h"

#include <memory>
#include <functional>

#include <QFont>

namespace Xybrid::UI {
    class SelectorGadget : public Gadget {
        Q_OBJECT

    public:
        typedef std::pair<int, QString> Entry;
    private:
        QFont font;
        qreal width = 128;
        Entry _entry = {-1, ""};

    public:
        std::function<std::vector<Entry>()> fGetList;
        std::function<void(QMenu*)> fEditMenu;

        SelectorGadget(QGraphicsItem* parent = nullptr);
        ~SelectorGadget() override = default;

        QRectF boundingRect() const override;
        void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

        void mousePressEvent(QGraphicsSceneMouseEvent*) override;

        void hoverEnterEvent(QGraphicsSceneHoverEvent*) override;
        void hoverLeaveEvent(QGraphicsSceneHoverEvent*) override;

        inline SelectorGadget* setWidth(qreal w) { width = w; update(); return this; }

        inline const Entry& entry() const { return _entry; }
        inline SelectorGadget* setEntry(const Entry& e, bool signal = true) {
            _entry = e;
            update();
            if (signal) emit onSelect(_entry);
            return this;
        }

        inline SelectorGadget* setListFunc(const std::function<std::vector<Entry>()>& f) { fGetList = f; return this; }
        inline SelectorGadget* setEditFunc(const std::function<void(QMenu*)>& f) { fEditMenu = f; return this; }

    signals:
        void onSelect(const Xybrid::UI::SelectorGadget::Entry&);
    };
}
