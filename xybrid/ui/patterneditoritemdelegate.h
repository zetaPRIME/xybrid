#pragma once
#include <QStyledItemDelegate>

namespace Xybrid::UI {
    class PatternEditorItemDelegate : public QStyledItemDelegate {
        Q_OBJECT
    public:
        PatternEditorItemDelegate(QWidget *parent = nullptr) : QStyledItemDelegate(parent) {}

        void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
        QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
        bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;

    protected:
        bool eventFilter(QObject *obj, QEvent *event) override;
        //QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
        //void setEditorData(QWidget *editor, const QModelIndex &index) const override;
        //void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

    signals:
        void update(const QModelIndex& index);
    };
}
