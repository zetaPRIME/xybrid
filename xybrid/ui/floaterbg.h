#pragma once

#include <QWidget>

namespace Xybrid::UI {
    class FloaterBG : public QWidget {
        Q_OBJECT
        // this is really just here for QSS purposes
    public:
        FloaterBG(QWidget* parent);
        ~FloaterBG() override = default;
    };
}
