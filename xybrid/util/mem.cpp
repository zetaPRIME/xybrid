#include "mem.h"

using namespace Xybrid::Util;

pmr::synchronized_pool_resource Xybrid::Util::rpool; // instantiate
decltype(Xybrid::Util::ralloc) Xybrid::Util::ralloc(&rpool);

namespace {
    static bool reserved = false;
}

void Xybrid::Util::reserveInitialPool() {
    if (reserved) return;
    reserved = true;

    const constexpr size_t rsize = 1024*1024*128;
    size_t bsize = rpool.options().largest_required_pool_block;
    std::vector<void*> allocs;
    for (size_t ts = 0; ts < rsize; ts += bsize) {
        allocs.push_back(rpool.allocate(bsize));
    }
    for (auto r : allocs) rpool.deallocate(r, bsize);
}
