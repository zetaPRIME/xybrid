#pragma once

#include <functional>
#include <QObject>

class LambdaEventFilter : public QObject {
    Q_OBJECT
    std::function<bool(QObject*, QEvent*)> filter;
public:
    LambdaEventFilter(QObject* parent, const std::function<bool(QObject*, QEvent*)>& f) : QObject(parent), filter(f) { }
    bool eventFilter(QObject* watched, QEvent* event) override { return filter(watched, event); }
    static inline LambdaEventFilter* create(QObject* parent, const std::function<bool(QObject*, QEvent*)>& f) { return new LambdaEventFilter(parent, f); }
};
