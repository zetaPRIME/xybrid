// miscellaneous "language extensions"
#pragma once

#include <type_traits>

// gcc only, no clang to complain here pls
#if defined(__GNUC__) && !defined(__clang__)
#define force_opt __attribute__((optimize("3")))
#endif

// things that work for gcc and clang
#if defined(__GNUC__) || defined(__clang__)
#define force_inline [[gnu::always_inline]]
#endif

// and empty defs for these if not available for current compiler
#ifndef force_inline
#define force_inline
#endif

#ifndef force_opt
#define force_opt
#endif

template<typename TO, typename FROM> inline constexpr TO hard_cast(FROM f) {
    static_assert(std::is_pointer_v<TO> && std::is_pointer_v<FROM>);
    return reinterpret_cast<TO>( reinterpret_cast<void*>(f) );
}
