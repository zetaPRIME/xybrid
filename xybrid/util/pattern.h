#pragma once

namespace Xybrid::Util {
    static constexpr int paramSoftCap = 16; // maximum number of parameter columns that can be displayed per channel; the rest are hidden
    static constexpr int colsPerChannel = 2 + (2 * paramSoftCap);

    inline int channelForColumn(int col) { return (col - (col % colsPerChannel)) / colsPerChannel; }
    inline int columnWithinChannel(int col) { return col % colsPerChannel; }

    inline int fieldForColumn(int col) {
        int cc = columnWithinChannel(col);
        if (cc < 2) return cc;
        return 2 + (cc-2) / 2;
    }
}
