#pragma once

#include <cstdint>

#include <QKeySequence>

namespace Xybrid::Util {
    int16_t keyToNote(int key);
    int unshiftedKey(int key);

    inline bool isModifier(int k) {
        return
            k == Qt::Key_Shift ||
            k == Qt::Key_Control ||
            k == Qt::Key_Alt ||
            k == Qt::Key_AltGr;
    }
}
