#pragma once

#include <QString>
#include <QStringBuilder>
#include <QVariant>

#define qs QStringLiteral

// we only need this one
namespace Xybrid::Config::UIConfig { extern bool modplugOctaveNotation; }

namespace Xybrid::Util {
    template<typename Num> inline QString numAndName(Num num, const QString& name) {
        if (name.isEmpty()) return QString::number(num);
        return qs("%1 (\"%2\")").arg(num).arg(name);
    }

    inline QString hex(int num, int fw = 2) {
        return qs("%1").arg(num, fw, 16, QChar('0')).toUpper();
    }

    template <typename T>
    inline QString enumName(T t) { return QVariant::fromValue(t).toString(); }

    inline QString sampleLength(int rate, int length) {
        double rSec = static_cast<double>(length) / static_cast<double>(rate);
        int tSec = static_cast<int>(rSec);
        int sec = tSec % 60;
        int min = (tSec - sec) / 60;
        int sub = static_cast<int>(rSec * 100) % 100;
        QChar zero('0');
        return qs("%1:%2.%3").arg(min, 2, 10, zero).arg(sec, 2, 10, zero).arg(sub, 2, 10, zero);
    }

    inline const constexpr char notemap[] = "C-C#D-D#E-F-F#G-G#A-A#B-";
    inline const constexpr char octmap[] = "=0123456789ABCDEF!";
    inline QString noteName(int16_t note) {
        if (note == -1) return " - ";
        if (note == -2) return " ^ ";
        if (note == -3) return " x ";
        auto nn = note % 12;
        auto oc = (note - nn) / 12;
        if (const constexpr int m = sizeof(octmap) - 1; oc > m) oc = m;
        else if (Config::UIConfig::modplugOctaveNotation) ++oc;
        char str[] = {notemap[nn*2], notemap[nn*2+1], octmap[oc], 0};
        return {str};
    }
}
