#include "keys.h"

#include <unordered_map>

#include <QKeyEvent>

namespace {
    std::unordered_map<int, int> shiftMap = [] {
        std::unordered_map<int, int> m;
        m[Qt::Key_BraceLeft] = Qt::Key_BracketLeft;
        m[Qt::Key_BraceRight] = Qt::Key_BracketRight;
        m[Qt::Key_Bar] = Qt::Key_Backslash;
        m[Qt::Key_Colon] = Qt::Key_Semicolon;
        m[Qt::Key_QuoteDbl] = Qt::Key_Apostrophe;
        m[Qt::Key_Less] = Qt::Key_Comma;
        m[Qt::Key_Greater] = Qt::Key_Period;
        m[Qt::Key_Question] = Qt::Key_Slash;
        return m;
    }();

    std::unordered_map<int, int16_t> keyMap = [] {
        std::unordered_map<int, int16_t> m;

        int16_t n = 12*4;
        int pianoKeys[] = {
            Qt::Key_Q, Qt::Key_W, Qt::Key_E, Qt::Key_R, Qt::Key_T, Qt::Key_Y, Qt::Key_U, Qt::Key_I, Qt::Key_O, Qt::Key_P, Qt::Key_BracketLeft, Qt::Key_BracketRight,
            Qt::Key_A, Qt::Key_S, Qt::Key_D, Qt::Key_F, Qt::Key_G, Qt::Key_H, Qt::Key_J, Qt::Key_K, Qt::Key_L, Qt::Key_Semicolon, Qt::Key_Apostrophe, Qt::Key_Backslash,
            Qt::Key_Z, Qt::Key_X, Qt::Key_C, Qt::Key_V, Qt::Key_B, Qt::Key_N, Qt::Key_M, Qt::Key_Comma, Qt::Key_Period, Qt::Key_Slash,
        };
        for (auto i : pianoKeys) m[i] = n++;

        // shift fix
        m[Qt::Key_BraceLeft] = m[Qt::Key_BracketLeft];
        m[Qt::Key_BraceRight] = m[Qt::Key_BracketRight];
        m[Qt::Key_Bar] = m[Qt::Key_Backslash];
        m[Qt::Key_Colon] = m[Qt::Key_Semicolon];
        m[Qt::Key_QuoteDbl] = m[Qt::Key_Apostrophe];
        m[Qt::Key_Less] = m[Qt::Key_Comma];
        m[Qt::Key_Greater] = m[Qt::Key_Period];
        m[Qt::Key_Question] = m[Qt::Key_Slash];

        return m;
    }();
}

int16_t Xybrid::Util::keyToNote(int key) {
    if (auto f = keyMap.find(key); f != keyMap.end()) return f->second;
    return -1; // default to none
}

int Xybrid::Util::unshiftedKey(int key) {
    if (auto f = shiftMap.find(key); f != shiftMap.end()) return f->second;
    return key;
}
