#pragma once

#include <cstddef>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>

#if defined(BOOST_PMR)
#include <boost/container/pmr/memory_resource.hpp>
#include <boost/container/pmr/polymorphic_allocator.hpp>
#include <boost/container/pmr/synchronized_pool_resource.hpp>
namespace pmr {
    using boost::container::pmr::synchronized_pool_resource;
    using boost::container::pmr::polymorphic_allocator;

    template<class T> using vector = std::vector<T, polymorphic_allocator<T>>;
    template<class T> using deque = std::deque<T, polymorphic_allocator<T>>;
    template<class T> using list = std::list<T, polymorphic_allocator<T>>;
    template<class T> using forward_list = std::forward_list<T, polymorphic_allocator<T>>;

    template <class Key, class Compare = std::less<Key>>
    using set = std::set<Key, Compare, polymorphic_allocator<Key>>;
    template <class Key, class Compare = std::less<Key>>
    using multiset = std::multiset<Key, Compare, polymorphic_allocator<Key>>;
    template <class Key, class T, class Compare = std::less<Key>>
    using map = std::map<Key, T, Compare, polymorphic_allocator<std::pair<const Key,T>>>;
    template <class Key, class T, class Compare = std::less<Key>>
    using multimap = std::multimap<Key, T, Compare, polymorphic_allocator<std::pair<const Key,T>>>;

    template <class Key, class Hash = std::hash<Key>, class Pred = std::equal_to<Key>>
    using unordered_set = std::unordered_set<Key, Hash, Pred, polymorphic_allocator<Key>>;
    template <class Key, class Hash = std::hash<Key>, class Pred = std::equal_to<Key>>
    using unordered_multiset = std::unordered_multiset<Key, Hash, Pred, polymorphic_allocator<Key>>;
    template <class Key, class T, class Hash = std::hash<Key>, class Pred = std::equal_to<Key>>
    using unordered_map = std::unordered_map<Key, T, Hash, Pred, polymorphic_allocator<std::pair<const Key,T>>>;
    template <class Key, class T, class Hash = std::hash<Key>, class Pred = std::equal_to<Key>>
    using unordered_multimap = std::unordered_multimap<Key, T, Hash, Pred, polymorphic_allocator<std::pair<const Key,T>>>;
}
//#include <boost/container/vector.hpp>
//using boost::container::vector;
#else
#include <memory_resource>
namespace pmr {
    using std::pmr::synchronized_pool_resource;
    using std::pmr::polymorphic_allocator;

    using std::pmr::vector;
    using std::pmr::deque;
    using std::pmr::list;
    using std::pmr::forward_list;

    using std::pmr::set;
    using std::pmr::multiset;
    using std::pmr::map;
    using std::pmr::multimap;

    using std::pmr::unordered_set;
    using std::pmr::unordered_multiset;
    using std::pmr::unordered_map;
    using std::pmr::unordered_multimap;
}
#endif

namespace Xybrid::Util {
    extern pmr::synchronized_pool_resource rpool;
    extern pmr::polymorphic_allocator<std::max_align_t> ralloc;

    void reserveInitialPool();

    template<typename T, size_t PSIZE = 16>
    class ResourcePool {
        typedef char slot[sizeof(T)];
        std::list<slot[PSIZE]> pages;

        std::deque<T*> avail;
        pmr::unordered_set<T*> used = {rpool};

        void newPage() {
            auto p = pages.emplace_back();
            for (size_t i = 0; i < PSIZE; i++) avail.push_back(&p[i]);
            //avail.res
        }

    public:
        typedef T type;
        static const constexpr size_t pageSize = PSIZE;

        template<typename... Arg>
        T* getNew(Arg... args) {
            if (avail.empty()) newPage();
            T* n = avail.pop_back();
            new (n) T(args...);
            used.insert(n);
            return n;
        }

        void del(T* n) {
            if (!used.contains(n)) return; // not mine
            n->~T(); // destroy object
            used.erase(n);
            avail.push_back(n);
        }

    };
}
