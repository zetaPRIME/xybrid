#pragma once
#if defined(ENABLE_LV2)

#include <memory>

#include <lilv/lilvmm.hpp>

// hacky macros to make working with lilvmm iterators less of a pain
#define foreach_node(EXP) if (Lilv::Nodes _fe = EXP; true) for (auto _fei = _fe.begin(); !_fe.is_end(_fei); _fei = _fe.next(_fei)) if (auto n = _fe.get(_fei); true)

namespace Lilv { struct World; }

namespace Xybrid::LV2 {
    extern std::unique_ptr<Lilv::World> world;

    inline Lilv::Node str(const char* s) { return world->new_string(s); }
    inline Lilv::Node uri(const char* s) { return world->new_uri(s); }
}

#endif
