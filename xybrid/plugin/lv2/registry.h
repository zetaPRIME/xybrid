#pragma once
#if defined(ENABLE_LV2)

#include <memory>
#include <vector>
#include <deque>
#include <unordered_set> // rem
#include <unordered_map>

#include <lilv/lilvmm.hpp>

namespace Lilv { struct World; struct Plugin; }

namespace Xybrid::LV2 {
    extern std::unique_ptr<Lilv::World> world;

    uint32_t mapURID(const std::string&);
    const std::string& unmapURID(uint32_t);
    void aliasURID(const std::string& base, const std::string& alias);

    class Features {
    public:
        static std::unordered_map<std::string, size_t> supported;
        static std::deque<LV2_Feature> master;

        static inline bool isSupported(const std::string& uri) { return supported.contains(uri); }

        std::vector<LV2_Feature*> ptrs;
        std::deque<LV2_Feature> store;

        Features();
        ~Features() = default;

        void setData(const std::string&, void*);
        //
    };

    class PluginDef;
    class PortDef {
    public:
        enum Direction : char { Input, Output };
        enum DataType : char {
            Value, ValueBuf, Atom
        };
        enum PortType : char {
            Audio, Control, CV,
            AtomBase, MIDI, XybridCommand,
        };
        enum Channel : char { NoChannel, LeftChannel, RightChannel };

        PluginDef* parent;

        PortType type = (PortType)-1;
        DataType dataType = (DataType)-1;
        Direction dir = (Direction)-1;
        Channel ch = NoChannel;

        size_t index;
        std::string symbol;
        std::string name;

        bool optional = false;
        bool toggled = false;
        bool hidden = false;
    };

    class PluginDef {
    public:
        PluginDef(const Lilv::Plugin& p) : lp(p) { }

        Lilv::Plugin lp;

        std::string uri;
        std::string name;

        std::vector<PortDef> ports;
        std::unordered_map<std::string, PortDef*> portsBySymbol;
        std::unordered_map<PortDef::Direction, std::unordered_map<PortDef::PortType, std::vector<PortDef*>>> portsByType;
        //
    };

    extern std::unordered_map<std::string, std::shared_ptr<PluginDef>> plugins;
}
#endif
