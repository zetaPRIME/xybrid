#pragma once

namespace Xybrid::LV2 {
#if defined(ENABLE_LV2)
    void init();
#else
    inline void init() { }
#endif
}
