#if defined(ENABLE_LV2)
#include "init.h"
#include "registry.h"
#include "util.h"

#include <string>
#include <memory>

#include <QDebug>

extern "C" {
#include "lv2/atom/atom.h"
#include "lv2/port-props/port-props.h"
#include "lv2/port-groups/port-groups.h"
#include "lv2/midi/midi.h"

#include "lv2/buf-size/buf-size.h"
#include "lv2/urid/urid.h"
}

#include <suil/suil.h>

#include "util/mem.h"

using namespace Xybrid;
using namespace Xybrid::LV2;

decltype(LV2::world) LV2::world = nullptr;
decltype(LV2::plugins) LV2::plugins;

namespace { // clazy:excludeall=non-pod-global-static
    LV2_URID_Map uridMap = [] {
        LV2_URID_Map m;
        m.handle = nullptr;
        m.map = [](void*, const char* uri) { return LV2::mapURID(uri); };
        return m;
    }();
    LV2_URID_Unmap uridUnmap = [] {
        LV2_URID_Unmap m;
        m.handle = nullptr;
        m.unmap = [](void*, uint32_t urid) { return LV2::unmapURID(urid).c_str(); };
        return m;
    }();

    void registerFeature(const std::string& uri, void* data = nullptr) {
        //if (Features::supported.contains(uri)) return;
        auto idx = Features::master.size();
        auto stri = Features::supported.emplace(uri, idx);
        auto& ft = Features::master.emplace_back();

        ft.URI = stri.first->first.c_str();
        ft.data = data;
    }

    void registerFeatures() {
        //

        registerFeature(LV2_BUF_SIZE__boundedBlockLength); // remember to actually follow this
        registerFeature(LV2_URID__map, &uridMap);
        registerFeature(LV2_URID__unmap, &uridUnmap);

        //
    }

    struct Lv2PlugInfo {
        QString uri;
        QString name;
    };
    std::unordered_map<QString, std::shared_ptr<Lv2PlugInfo>> pinfo;

    void loadPlugin(Lilv::Plugin p) {
        auto pi = std::make_shared<PluginDef>(p);
        pi->uri = p.get_uri().as_string();
        pi->name = p.get_name().as_string();

        // create submaps
        for (auto d : {PortDef::Input, PortDef::Output}) {
            auto& dm = pi->portsByType.try_emplace(d).first->second;
            for (auto t : {PortDef::Audio, PortDef::Control, PortDef::CV, PortDef::AtomBase}) {
                dm.try_emplace(t);
            }
        }

        bool abort = false;

        foreach_node(p.get_required_features()) {
            auto s = n.as_string();
            if (!Features::isSupported(s)) {
                qWarning() << "[LV2] X Plugin" << pi->name.c_str() << "requires unsupported feature:" << s;
                abort = true; // return after displaying all unmet-requirement warnings
            }
        }
        if (abort) return;

        auto np = p.get_num_ports();
        pi->ports.reserve(np);
        for (unsigned int i = 0; i < np; ++i) {
            auto pt = p.get_port_by_index(i);
            auto& ptd = pi->ports.emplace_back();
            ptd.parent = pi.get();
            ptd.index = i;
            ptd.symbol = Lilv::Node(pt.get_symbol()).as_string();
            ptd.name = Lilv::Node(pt.get_name()).as_string();
            pi->portsBySymbol[ptd.symbol] = &ptd;

            foreach_node(pt.get_classes()) { // determine direction and type
                std::string c = n.as_string();
                /**/ if (c == LV2_CORE__InputPort) ptd.dir = PortDef::Input;
                else if (c == LV2_CORE__OutputPort) ptd.dir = PortDef::Output;
                else if (c == LV2_CORE__AudioPort) ptd.type = PortDef::Audio;
                else if (c == LV2_CORE__CVPort) ptd.type = PortDef::CV;
                else if (c == LV2_CORE__ControlPort) ptd.type = PortDef::Control;
                else if (c == LV2_ATOM__AtomPort) ptd.type = PortDef::AtomBase;

                // log any unknown classes
                else qDebug() << "[LV2]  " << pi->name.c_str() << "port" << i << "has unknown port class" << c.c_str();
            }

            // abort if we don't know what a port is/needs to be hooked to
            if (ptd.dir == -1) {
                qWarning() << "[LV2] X" << pi->name.c_str() << "port" << i << "has no recognized direction";
                abort = true;
            }
            if (ptd.type == -1) {
                qWarning() << "[LV2] X" << pi->name.c_str() << "port" << i << "has no recognized type";
                abort = true;
            }
            if (abort) return;

            pi->portsByType[ptd.dir][ptd.type].push_back(&ptd); // add to relevant list

            // determine backing type
            if (ptd.type == PortDef::Audio || ptd.type == PortDef::CV) ptd.dataType = PortDef::ValueBuf;
            else if (ptd.type == PortDef::Control) ptd.dataType = PortDef::Value;
            else if (ptd.type == PortDef::AtomBase) ptd.dataType = PortDef::Atom;

            foreach_node(pt.get_value(uri(LV2_CORE__designation))) {
                std::string d = n.as_string();
                if (d == LV2_CORE__control) ; // we know what this is and don't care to do anything with it
                else if (d == LV2_PORT_GROUPS__left) ptd.ch = PortDef::LeftChannel;
                else if (d == LV2_PORT_GROUPS__right) ptd.ch = PortDef::RightChannel;
                else qDebug() << "[LV2]  " << pi->name.c_str() << "port" << i << "has unknown designation" << d.c_str();
            }

            foreach_node(pt.get_properties()) {
                std::string p = n.as_string();
                if (p == LV2_CORE__connectionOptional) ptd.optional = true;
                else if (p == LV2_CORE__toggled) ptd.toggled = true;
                else if (p == LV2_PORT_PROPS__notOnGUI) ptd.hidden = true;
                else qDebug() << "[LV2]  " << pi->name.c_str() << "port" << i << "has unknown property" << p.c_str();
            }

            if (ptd.type == PortDef::AtomBase) { // check what event types
                foreach_node(pt.get_value(uri(LV2_ATOM__supports))) {
                    std::string s = n.as_string();
                    PortDef::PortType t = PortDef::AtomBase;
                    if (s == LV2_MIDI__MidiEvent) t = PortDef::MIDI;
                    //else qDebug() << ">LV2>" << pi->name.c_str() << "port" << i << "supports:" << n.as_string();
                    if (t > ptd.type) ptd.type = t;
                }
            }
            //
        }

        qDebug() << "[LV2] + Plugin" << pi->name.c_str() << "loaded successfully";

        // save into loaded info
        plugins[pi->uri] = pi;
        // TODO register plugin
    }
}

void LV2::init() {
    if (world) return;
    world.reset(new Lilv::World());
    registerFeatures();
    world->load_all();

    auto pl = world->get_all_plugins();

    for (auto pi = pl.begin(); !pl.is_end(pi); pi = pl.next(pi)) loadPlugin(pl.get(pi));

    // premap needed URIs
    mapURID(LV2_MIDI__MidiEvent);
}
//

#endif

