#if defined(ENABLE_LV2)
#include "registry.h"

using namespace Xybrid;
using namespace Xybrid::LV2;

namespace { // clazy:excludeall=non-pod-global-static
    uint32_t uridCtr = 0;
    std::unordered_map<std::string, uint32_t> uridMap;
    std::unordered_map<uint32_t, std::string> uridUnmap;
    std::string nullStr;
}

uint32_t LV2::mapURID(const std::string& str) {
    if (auto it = uridMap.find(str); it != uridMap.end()) return it->second;
    auto urid = uridCtr++;
    uridMap[str] = urid;
    uridUnmap[urid] = str;
    return urid;
}

const std::string& LV2::unmapURID(uint32_t urid) {
    if (auto it = uridUnmap.find(urid); it != uridUnmap.end()) return it->second;
    return nullStr;
}

void LV2::aliasURID(const std::string& base, const std::string& alias) {
    auto urid = mapURID(base);
    uridMap[alias] = urid;
}

decltype(Features::supported) Features::supported;
decltype(Features::master) Features::master;

Features::Features() {
    ptrs.reserve(master.size() + 1);
    for (auto& f : master) {
        ptrs.push_back(&f);
    }
    ptrs.push_back(nullptr);
}

void Features::setData(const std::string& uri, void * d) {
    if (auto it = supported.find(uri); it != supported.end()) { // find index of feature in master record
        auto idx = it->second;
        auto fp = ptrs[idx]; // pick up our reference
        if (fp == &master[idx]) fp = &store.emplace_back(*fp); // if still to master instance, copy to local
        fp->data = d; // and assign data pointer
    }
}
#endif
