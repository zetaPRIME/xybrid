#pragma once
#if defined(ENABLE_LV2)

#include <memory>
#include <vector>

#include "registry.h"

//#include <lilv/lilvmm.hpp>

namespace Lilv { struct Instance; }

namespace Xybrid::LV2 {
    class PluginWrapper;
    class PortBuffer {
    public:
        PluginWrapper* parent;
        PortDef* def;
        void* dataPtr;
        std::vector<float> buf;
    };

    class PluginWrapper {
    public:
        std::shared_ptr<PluginDef> def;
        std::vector<PortBuffer> ports;
        Features features;

        std::unique_ptr<Lilv::Instance> instance;
    };
}
#endif
