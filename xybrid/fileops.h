#pragma once

#include <memory>

#include <QFileInfo>

#include "config/directories.h"

namespace Xybrid::Data {
    class Project;
    class Graph;
    class Node;
}

namespace Xybrid::FileOps {
    namespace Filter {
        extern const QString project;
        extern const QString node;

        extern const QString audioIn;
        extern const QString audioOut;
    }
    QString showOpenDialog(QWidget* parent = nullptr, const QString& caption = QString(), const QString& directory = QString(), const QString& filter = QString());
    QString showSaveAsDialog(QWidget* parent = nullptr, const QString& caption = QString(), const QString& directory = QString(), const QString& filter = QString(), const QString& suffix = QString());

    bool saveProject(std::shared_ptr<Data::Project> project, QString fileName = QString());
    std::shared_ptr<Data::Project> loadProject(QString fileName, bool asTemplate = false);
    std::shared_ptr<Data::Project> newProject(bool useTemplate = true);

    bool saveNode(std::shared_ptr<Data::Node> node, QString fileName);
    std::shared_ptr<Data::Node> loadNode(QString fileName, std::shared_ptr<Data::Graph> parent = nullptr);

    void loadConfig();
    void saveConfig();

    void loadUIState();
    void saveUIState();

}
