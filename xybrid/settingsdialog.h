#pragma once

#include <QDialog>

#include <functional>

namespace Ui {
    class SettingsDialog;
}

namespace Xybrid {
    class SettingsDialog : public QDialog {
        Q_OBJECT

        std::vector<std::function<void()>> binds;

    public:
        static SettingsDialog* instance;

        explicit SettingsDialog(QWidget *parent = nullptr);
        ~SettingsDialog() override;

        static void tryOpen(bool focusAbout = false);

    public slots:

        void apply();
        void reject() override;

    private:
        Ui::SettingsDialog *ui;
    };


}
