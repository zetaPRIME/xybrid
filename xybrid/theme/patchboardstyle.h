#pragma once

#include <QRectF>

class QPainter;
class QStyleOptionGraphicsItem;

namespace Xybrid::Theme {
    class PatchboardStyle {
    public:
        PatchboardStyle() = default;
        virtual ~PatchboardStyle() = default;

        /// Draws the panel backing a NodeObject.
        virtual void drawPanel(QPainter*, const QStyleOptionGraphicsItem*, const QRectF&, double corner [[maybe_unused]] = 1.0) { }
        /// Draws the panel variant used for a PortConfigObject.
        virtual void drawFloaterPanel(QPainter* p, const QStyleOptionGraphicsItem* opt, const QRectF& r) { drawPanel(p, opt, r); }

        /// SelectorGadget background
        virtual void drawSelectorBacking(QPainter*, const QStyleOptionGraphicsItem*, const QRectF&, bool arrow [[maybe_unused]] = true) { }
        /// ParamSelectorGadget, NoteSelectorGadget
        virtual void drawPatternSelectorBacking(QPainter*, const QStyleOptionGraphicsItem*, const QRectF&) { }

        //
    };
}
