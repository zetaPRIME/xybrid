#pragma once

#include "theme/patchboardstyle.h"

#include <QColor>
#include <QLinearGradient>

namespace Xybrid::Theme::Patchboard {
    class DefaultStyle : public PatchboardStyle {
    public:
        QColor outlineColor = {31, 31, 31};
        QColor selectionColor = {127, 127, 255};

        QLinearGradient entryBg;

        DefaultStyle();
        ~DefaultStyle() override = default;

        void drawPanel(QPainter*, const QStyleOptionGraphicsItem*, const QRectF&, double corner [[maybe_unused]]) override;

        void drawSelectorBacking(QPainter*, const QStyleOptionGraphicsItem*, const QRectF&, bool arrow [[maybe_unused]]) override;
        void drawPatternSelectorBacking(QPainter*, const QStyleOptionGraphicsItem*, const QRectF&) override;
    };
}
