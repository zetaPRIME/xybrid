#include "defaultstyle.h"
using Xybrid::Theme::Patchboard::DefaultStyle;

#include <cmath>

#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "config/colorscheme.h"
using Xybrid::Config::colorScheme;

namespace {
    QColor blend(const QColor& a, const QColor& b) {
        auto aa = b.alphaF();
        QColor o;
        o.setRedF(std::lerp(a.redF(), b.redF(), aa));
        o.setGreenF(std::lerp(a.greenF(), b.greenF(), aa));
        o.setBlueF(std::lerp(a.blueF(), b.blueF(), aa));
        return o;
    }
}

DefaultStyle::DefaultStyle() {
    entryBg.setColorAt(0.0, QColor(15, 15, 15));
    entryBg.setColorAt(0.75, QColor(15, 15, 15));
    entryBg.setColorAt(1.0, QColor(35, 35, 35));
}

void DefaultStyle::drawPanel(QPainter* p, const QStyleOptionGraphicsItem* opt, const QRectF& r, double corner) {
    double rad = 8.0 * corner;
    QColor outline = outlineColor;
    if (opt->state & QStyle::State_Selected) outline = selectionColor;

    QLinearGradient fill(QPointF(0, 0), QPointF(0, r.height()));
    fill.setColorAt(0, QColor(95, 95, 95));
    fill.setColorAt(16.0/r.height(), QColor(63, 63, 63));
    fill.setColorAt(1.0 - (1.0 - 16.0/r.height()) / 2, QColor(55, 55, 55));
    fill.setColorAt(1, QColor(35, 35, 35));

    p->setRenderHint(QPainter::RenderHint::Antialiasing);
    p->setBrush(QBrush(fill));
    p->setPen(QPen(QBrush(outline), 2));
    p->drawRoundedRect(r, rad, rad);
}

void DefaultStyle::drawSelectorBacking(QPainter* p, const QStyleOptionGraphicsItem* opt, const QRectF& r, bool arrow) {
    bool hover = opt->state & QStyle::State_MouseOver;

    QColor outline = QColor(31, 31, 31);
    if (hover) outline = QColor(127, 127, 255);

    auto fill = entryBg;
    fill.setFinalStop(r.bottomLeft());

    p->setRenderHint(QPainter::RenderHint::Antialiasing);
    p->setBrush(QBrush(fill));
    p->setPen(QPen(QBrush(outline), 2));
    p->drawRoundedRect(r, 8, 8);

    if (arrow) {
        p->setPen(QPen(Qt::NoPen));
        p->setBrush(outline.lighter(200));
        qreal tw = 6;
        QPointF tc = {r.right() - 4 - tw/2, r.center().y()};
        p->drawPolygon(QPolygonF({tc + QPointF(-tw/2, -tw/2), tc + QPointF(tw/2, -tw/2), tc + QPointF(0, tw/2)}));
    }
}

void DefaultStyle::drawPatternSelectorBacking(QPainter* p, const QStyleOptionGraphicsItem* opt, const QRectF& r) {
    bool hover = opt->state & QStyle::State_MouseOver;
    bool focus = opt->state & QStyle::State_HasFocus;

    QColor outline = outlineColor;
    if (focus) outline = selectionColor;
    if (hover) outline = blend(outline, QColor(255, 255, 255, 63)); //outline = QColor(71, 71, 71);

    QLinearGradient fill(r.topLeft(), r.bottomLeft());
    fill.setColorAt(0.0, colorScheme.patternBg);
    fill.setColorAt(0.5, colorScheme.patternBgMeasure);
    fill.setColorAt(1.0, colorScheme.patternBg);

    p->setRenderHint(QPainter::RenderHint::Antialiasing);
    p->setBrush(QBrush(fill));
    p->setPen(QPen(QBrush(outline), 2));
    p->drawRoundedRect(r, 8, 8);
}

