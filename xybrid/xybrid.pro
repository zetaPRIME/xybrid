#-------------------------------------------------
#
# Project created by QtCreator 2018-11-19T00:10:46
#
#-------------------------------------------------

QT       += core gui multimedia opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = xybrid
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050F00

DISTFILES += ../.astylerc

CONFIG += c++2a
# shut up warnings about qsizepolicy and only qsizepolicy
QMAKE_CXXFLAGS += -Wno-deprecated-enum-enum-conversion

# use all optimizations that won't generally interfere with debugging
QMAKE_CXXFLAGS_DEBUG += -Og
# QMAKE_CXXFLAGS_RELEASE += -O3

# automatically build file lists
SOURCES += $$files(*.cpp, true)
HEADERS += $$files(*.h, true) \
           $$files(*.hpp, true)
FORMS += $$files(*.ui, true)
RESOURCES += res/resources.qrc

# features enabled by default
CONFIG +=

# features in early work, enabled only in debug
CONFIG(debug) {
    CONFIG += lv2
}

lv2 { # lv2 support
    DEFINES += ENABLE_LV2
    linux {
        LIBS += -llilv-0 -lsuil-0
        INCLUDEPATH += /usr/include/lilv-0 /usr/include/suil-0
    }
}



freebsd-clang {
    DEFINES += BOOST_PMR BOOST_BESSEL
    LIBS += -lboost_container -lboost_math_tr1
}

macx: {
    DEFINES += BOOST_PMR BOOST_BESSEL
    LIBS += -L/Users/rachel/.nix-profile/lib -lboost_container -lboost_math_tr1
    LIBS    += -framework OpenGL
    LIBS    += -framework Foundation
    SOURCES +=
    HEADERS +=
    QMAKE_CXXFLAGS += -g -I/nix/store/gp7ascpyzsprb2i79kl5a22rmyawwscb-boost-1.77.0-dev/include
}


# TODO: make this work.
CONFIG (boost) {
    DEFINES += BOOST_PMR BOOST_BESSEL
    LIBS += -L$${BOOSTPATH}
    QMAKE_CXXFLAGS += -I$${BOOSTINCLUDE}
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

media.path = /opt/$${TARGET}
media.files += icon32.png
INSTALLS += media

desktop.path = /usr/share/applications
desktop.files += xybrid.desktop
INSTALLS += desktop

mime.path = /usr/share/mime/packages
mime.files += xybrid.xml
INSTALLS += mime
