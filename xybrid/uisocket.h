#pragma once

#include <QObject>

class QUndoStack;

namespace Xybrid::Data {
    class Project;
    class Pattern;
    class Graph;
    class Node;
}

namespace Xybrid {
    class MainWindow;
    class UISocket : public QObject {
        Q_OBJECT
    public:
        MainWindow* window;
        QUndoStack* undoStack;

    signals:
        void updatePatternLists();
        void patternUpdated(Data::Pattern* pattern, int level);
        void rowUpdated(Data::Pattern* pattern, int channel, int row);

        void sampleListUpdated();

        void openGraph(Data::Graph*);
        void openNodeUI(Data::Node*);
    };
}
