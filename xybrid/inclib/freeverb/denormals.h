// (originally) Macro for killing denormalled numbers
//
// Written by Jezar at Dreampoint, June 2000
// http://www.dreampoint.co.uk
// Based on IS_DENORMAL macro by Jon Watte
// This code is public domain

// Xybrid: replaced with equivalent template that will work on any precision; replaced include guards with #pragma once

#pragma once

#include <cmath>
#include <type_traits>
template <typename T> inline typename std::enable_if_t<std::is_floating_point_v<T>, void> undenormalise(T& sample) { if (!std::isnormal(sample)) sample = 0.0; }

// original macro as follows:
// #define undenormalise(sample) if(((*(unsigned int*)&sample)&0x7f800000)==0) sample=0.0f

//ends
